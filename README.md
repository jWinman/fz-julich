## Summer project: An active dumbbell in a mesoscopic, hydrodynamic simulation ##

# Content #
1. work: computer simulations
2. final_talk: last presentation from 28th September
3. talk: first draft presentation
4. scientific_report: scientific report
5. progress_report: report about the experience
6. papers: literature
7. Abstract: first abstract about the work (unfortunately completely wrong)