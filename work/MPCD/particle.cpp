#include <cmath>
#include <iostream>

#include "particle.h"
#include "tuple.h"

// --------------------- Auslesen der Position des Teilchens ----------------------

double Particle::GetPos(char pos)
{
	switch(pos)
	{
		case 'x': return _x;
		case 'y': return _y;
		case 'z': return _z;
		default: return 0;
	}
}

double Particle::Getv(char pos)
{
	switch(pos)
	{
		case 'x': return _v_x;
		case 'y': return _v_y;
		case 'z': return _v_z;
		default: return  0;
	}
}

// --------------------- Randbedingungen der Teilchen -----------------------------

void Particle::x_border(double xleft, double xright) //xright > x > xleft
{
	double l = fabs(xleft - xright);
	_x -= l * floor(_x / l);
}

void Particle::y_border(double ydown, double yup, double time)
{
	if (_y < ydown || _y > yup)
	{
		_v_x = -_v_x;
		_v_y = -_v_y;
		_v_z = -_v_z;
		streaming(time);
	}
}

void Particle::z_border(double zdown, double zup)
{
	double l = fabs(zdown - zup);
	_z -= l * floor(_z / l);
}

// --------------------- Streaming-Schritt & Beschleunigung -----------------------

void Particle::streaming(double time)
{
	_y += time * _v_y;
	_x += time * _v_x;
	_z += time * _v_z;
}

void Particle::acceleration(double time, double force_x)
{
	_v_x += force_x * time;
}

// --------------------- Kollision-Schritt und Shiften des Grids ------------------

void Particle::collisionSRD3D(double *v_mittel, double *R, double alpha, double kappa)
{
    double v_xtmp = _v_x,
    	   v_ytmp = _v_y,
    	   v_ztmp = _v_z;
    double R_x = R[0],
           R_y = R[1],
           R_z = R[2];
    ///Elemente der Matrix D_rot:
    double D_rot[3][3];
    D_rot[0][0] = R_x*R_x+(1.0-R_x*R_x)*cos(alpha);
    D_rot[0][1] = R_x*R_y*(1.0-cos(alpha)) - R_z*sin(alpha);
    D_rot[0][2] = R_x*R_z*(1.0-cos(alpha)) + R_y*sin(alpha);
    D_rot[1][0] = R_x*R_y*(1.0-cos(alpha)) + R_z*sin(alpha);
    D_rot[1][1] = R_y*R_y+(1.0-R_y*R_y)*cos(alpha);
    D_rot[1][2] = R_y*R_z*(1.0-cos(alpha)) - R_x*sin(alpha);
    D_rot[2][0] = R_x*R_z*(1.0-cos(alpha)) - R_y*sin(alpha);
    D_rot[2][1] = R_y*R_z*(1.0-cos(alpha)) + R_x*sin(alpha);
    D_rot[2][2] = R_z*R_z+(1.0-R_z*R_z)*cos(alpha);
    _v_x = v_mittel[0] + D_rot[0][0] * kappa * (v_xtmp - v_mittel[0])
    	               + D_rot[0][1] * kappa * (v_ytmp - v_mittel[1])
    	               + D_rot[0][2] * kappa * (v_ztmp - v_mittel[2]);
    _v_y = v_mittel[1] + D_rot[1][0] * kappa * (v_xtmp - v_mittel[0])
    	               + D_rot[1][1] * kappa * (v_ytmp - v_mittel[1])
    	               + D_rot[1][2] * kappa * (v_ztmp - v_mittel[2]);
    _v_z = v_mittel[2] + D_rot[2][0] * kappa * (v_xtmp - v_mittel[0])
    	               + D_rot[2][1] * kappa * (v_ytmp - v_mittel[1])
    	               + D_rot[2][2] * kappa * (v_ztmp - v_mittel[2]);
}

void Particle::shift(tuple<double> a0, double xleft, double xright, double zdown, double zup)
{
	double l_x = fabs(xleft - xright);
	double l_z = fabs(zdown - zup);
	_x += a0.x;
	if (_x < xleft) _x += l_x;
	else if (_x > xright) _x -= l_x;
	_y += a0.y;
	_z += a0.z;
	if (_z < zdown) _z += l_z;
	else if (_z > zup) _z -= l_z;
}
