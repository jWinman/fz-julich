#include <iostream>
#include <cstdio>
#include <cmath>
#include <functional>
#include <random>
#include <cmath>

#include "cellgrid.h"
#include "particle.h"

std::random_device rd1;
std::mt19937 generator1(rd1());
std::uniform_real_distribution<double> distribution1(0.,1.);
std::normal_distribution<double> normaldistribution(0., 1.);
auto Gauss1 = bind(normaldistribution, ref(generator1));
auto Mersenne1 = bind(distribution1, ref(generator1));

void printMeanVelocity(tuple<double> size, double ****mean_xyz, double ***particle_dens)
{
	printf("Output\n");
	char name1[100];
	char name2[100];
	sprintf(name1, "Daten/velocity.csv");
	sprintf(name2, "Daten/dichte.csv");
	FILE *file1 = fopen(name1, "w");
	FILE *file2 = fopen(name2, "w");
	for (int i = 0; i < int(size.x); i++)
	{
		for (int j = 0; j < int(size.y) + 1; j++)
		{
		    double particle_dens2D = 0;
		    double mean_xy[2] = {0, 0};
		    for (int l = 0; l < int(size.z); l++)
		    {
			//fprintf(file1, "%d %d %d %.15f %.15f %.15f \n", i, j, l, mean_xyz[i][j][l][0], mean_xyz[i][j][l][1], mean_xyz[i][j][l][2]);
			particle_dens2D += particle_dens[i][j][l] / size.z;
			mean_xy[0] += mean_xyz[i][j][l][0] / size.z;
			mean_xy[1] += mean_xyz[i][j][l][1] / size.z;
		    }
		    fprintf(file2, "%.2f ", particle_dens2D);
		    fprintf(file1, "%d %d %.15f %.15f\n", i, j,mean_xy[0], mean_xy[1]);

		}
		fprintf(file2, "\n");
	}
	fclose(file1);
	fclose(file2);
}

void printVelocityDistributions(Particle *teilchenarray, int NP, int w)
{
		printf("Output von %d \n", w);
		char velocity_distribution[100];
		char pos_distribution[100];
		sprintf(velocity_distribution, "Daten/Distributions/velocity_distribution-%d.csv", w);
		sprintf(pos_distribution, "Daten/Distributions/position_distribution-%d.csv", w);
		FILE *velocityDistribution = fopen(velocity_distribution, "w");
		FILE *posDistribution = fopen(pos_distribution, "w");
		for (int i = 0; i < NP; i++)
		{
			fprintf(velocityDistribution, "%.15f %.15f %.15f \n", teilchenarray[i].Getv('x'), teilchenarray[i].Getv('y'), teilchenarray[i].Getv('z'));
			fprintf(posDistribution, "%.15f %.15f %.15f \n", teilchenarray[i].GetPos('x'), teilchenarray[i].GetPos('y'), teilchenarray[i].GetPos('z'));
		}
		fclose(velocityDistribution);
		fclose(posDistribution);
}

Particle* createparticles(int N, tuple<double> size, double T)
{
	Particle *array = new Particle[N];
	for (int i = 0; i < N; i++)
	{
		array[i] = Particle(Mersenne1() * size.x, Mersenne1() * size.y, Mersenne1() * size.z, sqrt(T) * Gauss1(), sqrt(T) * Gauss1(), sqrt(T) * Gauss1(), 1);
	}
	return array;
}

void work(int NP, double T, double force, tuple<double> size, double measure_time, double delta_t, double ****mean_xyz, double ***particle_dens)
{
	// Teilchen und Zellen erstellen
	Particle *teilchenarray = createparticles(NP, size, T);
	CellGrid zellenarray(size, 1, NP);

	//mittlere Teilchenzahldichte
	int M = NP / (size.x * size.y * size.z);


	// Äquilibrierung
	for (int w = 0; w < measure_time; w++)
	{
		zellenarray.reset_all();
		zellenarray.acceleration_cell(teilchenarray, NP, delta_t, force);
		zellenarray.fill_up_particle_array(teilchenarray, NP, size.x, size.z);
		zellenarray.algorithm(size, delta_t, M, T);
		//printVelocityDistributions(teilchenarray, NP, w);

		for (int i = 0; i < int(size.x); i++)
		{
			for (int j = 0; j < int(size.y) + 1; j++)
			{
			    for (int l = 0; l < int(size.z); l++)
			    {
				mean_xyz[i][j][l][0] += zellenarray.Getux(i, j, l) / measure_time;
				mean_xyz[i][j][l][1] += zellenarray.Getuy(i, j, l) / measure_time;
				mean_xyz[i][j][l][2] += zellenarray.Getuz(i, j, l) / measure_time;
				particle_dens[i][j][l] += zellenarray.quantity(i, j, l) / (double) measure_time;
			    }
			}
		}
	}

	delete teilchenarray;
}

int main()
{
	// Randbedingungen etc. festlegen
	int NP = 10000;
	double T = .02;

	tuple<double> size = {10, 10, 10};

	int measure_time = 1000;
	double delta_t = 0.1;
	double force = 0.01;


	// zero-Arrays in denen Mittelwert/Teilchendichte der xyz-Geschwindigkeiten bei der Auslesephase gespeichert werden
	double ****mean_xyz = new double***[int(size.x)]();
	double ***particle_dens = new double**[int(size.x)]();
	for (int i = 0; i < int(size.x); i++)
	{
		mean_xyz[i] = new double**[int(size.y) + 1]();
		particle_dens[i] = new double*[int(size.y) + 1]();
		for (int j = 0; j < int(size.y) + 1; j++)
		{
			mean_xyz[i][j] = new double*[int(size.z)]();
			particle_dens[i][j] = new double[int(size.z)]();
			for (int l = 0; l < int(size.z); l++)
			{
			    mean_xyz[i][j][l] = new double[3]();
			}
		}
	}

	//Äquilibrierung u. Messung
	work(NP, T, force, size, measure_time, delta_t, mean_xyz, particle_dens);
	printMeanVelocity(size, mean_xyz, particle_dens);
	return 0;
}

//	// Ausleseteil:
//	for (int w = 0; w < measure_time; w++)
//	{
//		zellenarray.reset_all();
//		zellenarray.fill_up_particle_array(teilchenarray, NP, size.x);
//		zellenarray.acceleration_cell(teilchenarray, NP, delta_t, force);
//		zellenarray.mean(T, M);
//		zellenarray.algorithm(size, delta_t);
//		zellenarray.reset_particle_array();
//		zellenarray.fill_up_particle_array(teilchenarray, NP, size.x);
//		zellenarray.mean(T, M);
//		for (int i = 0; i < int(size.x); i++)
//		{
//			for (int j = 0; j < int(size.y) + 1; j++)
//			{
//			    for (int l = 0; l < int(size.z) + 1; l++)
//			    {
//				mean_xyz[i][j][l][0] += zellenarray.Getux(i, j, l) / measure_time;
//				mean_xyz[i][j][l][1] += zellenarray.Getuy(i, j, l) / measure_time;
//				mean_xyz[i][j][l][2] += zellenarray.Getuz(i, j, l) / measure_time;
//				particle_dens[i][j][l] += zellenarray.quantity(i, j, l) / (double) measure_time;
//			    }
//			}
//		}
//	}
