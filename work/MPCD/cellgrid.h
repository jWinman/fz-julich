#include "particle.h"
#include "tuple.h"

#ifndef CELL_H
#define CELL_H

class CellGrid
{
	private:
		int _Nx, _Ny, _Nz; // Anzahl der Zellen in xyz-Richtung
		int _a;  // Gitterkonstante und Abmessungen des Gitters
		int _N_Particle; // Gesamtanzahl der Teilchen

		Particle *****_particle_array; // Teilchen / Zelle in dem Gitter
		double ****_u; // Mittelwerte der Geschwindigkeiten
		double ****_R; // Werte für die Rotationsmatrix
		int ***_Particle_per_cell; // Anzahl der Teilchen pro Zelle
		
		tuple<double> _shift; // Werte für die Verschiebung des Gitters
	public:
		CellGrid(tuple<double> Nxyz, int a, int N_Particle); // Konstruktur
		void ghost_particles(int i, int j, int l, double T, double M); // berechnet Wandteilchen beim Mittelwert
		void reset_all(); // _shift, _R initialisieren und _Particle_per_cell(i, j) nullsetzen
		void reset_particle_array(); // nur _Particle_per_cell(i, j) nullsetzen
		void fill_up_particle_array(Particle *array, int length, double xright, double zup); // füllt _particle_array(i, j) mit Teilchen aus array
		void acceleration_cell(Particle *array, int length, double time, double force); // Beschleunigt alle Teilchen mit der Kraft force in x-Richtung
		void algorithm(tuple<double> size, double time, double M, double T); // führt den eigentlichen Algorithmus aus collision und streaming aus
		double isotherm_thermostate(int i, int j, int l, double T);
		double isokin_thermostate(int i, int j, int l, double T);
		double Getux(int i, int j, int l) {return _u[i][j][l][0];} // Mittelwert u_x der Zelle (i,j) zurückgeben
		double Getuy(int i, int j, int l) {return _u[i][j][l][1];} // Mittelwert u_y der Zelle (i,j) zurückgeben
		double Getuz(int i, int j, int l) {return _u[i][j][l][2];} // Mittelwert u_y der Zelle (i,j) zurückgeben
		int quantity(int i, int j, int l) {return _Particle_per_cell[i][j][l];}; // Anzahl an Teilchen in Zelle (i, j)
		~CellGrid(); // DESTRUCTION
};

#endif
