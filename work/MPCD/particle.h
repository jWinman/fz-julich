#include "tuple.h"

#ifndef PARTICLE_H
#define PARTICLE_H

class Particle 
{
	private:
		double _x, _y, _z, _mass;
		double _v_x, _v_y, _v_z;

	public:
		Particle() {};
		Particle(double x, double y, double z, double v_x, double v_y, double v_z, double mass) :
		    _x(x),
		    _y(y),
		    _z(z),
		    _mass(mass),
		    _v_x(v_x),
		    _v_y(v_y),
		    _v_z(v_z){}
		double GetPos(char pos);
		double Getv(char pos);
		double Getmass() {return _mass;}
		void x_border(double xleft, double xright);
		void y_border(double yleft, double yright, double time);
		void z_border(double zleft, double zright);
		void streaming(double time);
		void acceleration(double time, double force);
		void collisionSRD3D(double *v_mittel, double *R, double alpha, double kappa);
		void shift(tuple<double> a0, double xleft, double xright, double zdown, double zup);
};
#endif
