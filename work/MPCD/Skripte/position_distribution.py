import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

for i in range(0,100):
        fig = plt.figure()

        x, y, z = np.loadtxt("Daten/position_distribution-{}.csv".format(i), unpack = True)
        bin_array = [j for j in np.arange(0, 26, 0.06)]

        count_x, bin_x = np.histogram(x, bins=bin_array)
        print(sum(count_x))
        ax = fig.add_subplot(3, 1, 1)
        ax.plot(bin_x[:len(count_x)], count_x, "r-", label="$x$")
        ax.legend(loc="best")

        count_y, bin_y = np.histogram(y, bins=bin_array)
        print(sum(count_y))
        ax = fig.add_subplot(3, 1, 2)
        ax.plot(bin_y[:len(count_y)], count_y, "b-", label="$y$")
        ax.legend(loc="best")

        count_z, bin_z = np.histogram(z, bins=bin_array)
        print(sum(count_z))
        ax = fig.add_subplot(3, 1, 3)
        ax.plot(bin_z[:len(count_z)], count_z, "g-", label="$z$")
        ax.legend(loc="best")

        ax.set_ylabel("Häufigkeit")
        ax.set_xlabel("Position $x$")
        fig.savefig("Plots/position_distribution-{}.pdf".format(i))

x = np.linspace(-10, 10, 10000)
f = lambda x: np.sqrt(1) / np.sqrt(2 * np.pi) * np.exp(-0.5 * x**2)

