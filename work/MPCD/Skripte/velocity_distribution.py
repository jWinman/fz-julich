import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

momentum = np.zeros((100, 3))
kinE = np.zeros(100)

for i in range(0,100):

        ux, uy, uz = np.loadtxt("Daten/Distributions/velocity_distribution-{}.csv".format(i), unpack = True)
        x, y , z = np.loadtxt("Daten/Distributions/position_distribution-{}.csv".format(i), unpack=True)
        bin_array = [j for j in np.arange(-1, 1, 0.01)]

        # total momentum and kinetic energy
        momentum[i] = np.array([np.mean(ux), np.mean(uy), np.mean(uz)])
        kinE[i] = np.mean(ux**2 + uy**2 + uz**2)

        #histograms for velocity of all particles
        fig = plt.figure()
        count_ux, bin_x = np.histogram(ux, bins=bin_array)
        ax = fig.add_subplot(3, 1, 1)
        ax.plot(bin_x[:len(count_ux)], count_ux, "r-", label="$v_x$")
        ax.legend(loc="best")

        count_uy, bin_y = np.histogram(uy, bins=bin_array)
        ax = fig.add_subplot(3, 1, 2)
        ax.plot(bin_y[:len(count_uy)], count_uy, "b-", label="$v_y$")
        ax.legend(loc="best")

        count_uz, bin_z = np.histogram(uz, bins=bin_array)
        ax = fig.add_subplot(3, 1, 3)
        ax.plot(bin_z[:len(count_uz)], count_uz, "g-", label="$v_z$")
        ax.legend(loc="best")
        
        ax.set_ylabel(r"Haeufigkeit")
        ax.set_xlabel("Geschwindigkeit $v$")
        fig.savefig("Plots/Distributions/velocity_distribution-{}.pdf".format(i))


# Plot of momentum and kinetic energy
x = np.linspace(0, 100, 100)
fig2 = plt.figure()
ax = fig2.add_subplot(3, 1, 1)
ax.plot(x, momentum[:,0])
ax = fig2.add_subplot(3, 1, 2)
ax.plot(x, momentum[:,1])
ax = fig2.add_subplot(3, 1, 3)
ax.plot(x, momentum[:,2])
fig2.savefig("Plots/total_momentum.pdf")

fig4 = plt.figure()
ax = fig4.add_subplot(1, 1, 1)
ax.plot(x, kinE)
fig4.savefig("Plots/kinE.pdf")
