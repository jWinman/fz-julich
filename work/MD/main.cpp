#include<cmath>
#include<functional>
#include<random>

#include "particle.h"
#include "system.h"

std::random_device rd;
std::mt19937 generator(rd());

void printPosVel(int t, int NP, double ***velocity, double ***position)
{
	printf("Output1\n");
	for (int i = 0; i < t; i++)
	{
	    char name1[100];
      	    char name2[100];
	    sprintf(name1, "Daten/velocity%d.csv", i);
      	    sprintf(name2, "Daten/position%d.csv", i);
	    FILE *file1 = fopen(name1, "w");
      	    FILE *file2 = fopen(name2, "w");
      	    for (int j = 0; j < NP; j++)
      	    {
		fprintf(file1, "%.10f %.10f %.10f\n", velocity[i][j][0], velocity[i][j][1], velocity[i][j][2]);
      	    	fprintf(file2, "%.10f %.10f %.10f\n", position[i][j][0], position[i][j][1], position[i][j][2]);
      	    }
	    fclose(file1);
	    fclose(file2);
	}
}

void printMom(int t, double **P)
{
	printf("Output2\n");
	char name1[100];
	sprintf(name1, "Daten/momentum.csv");
	FILE *file1 = fopen(name1, "w");
	for (int i = 0; i < t; i++)
	{
	    	fprintf(file1, "%d %.10f %.10f %.10f\n", i, P[i][0], P[i][1], P[i][2]);
	}
	fclose(file1);
}

void printE(int t, double *Ekin, double *Epot)
{
	printf("Output3\n");
	char nameE[100];
	sprintf(nameE, "Daten/E.csv");
	FILE *file1 = fopen(nameE, "w");
	for (int i = 0; i < t; i++)
	{
	    	fprintf(file1, "%d %.10f %.10f %.10f\n", i, Ekin[i], Epot[i], Ekin[i] + Epot[i]);
	}
	fclose(file1);
}

Particle* createparticles(int N, tuple<double> size, double T, double l, double alpha)
{
	std::normal_distribution<double> gauss_distribution(0, 1.);
	auto Gauss = bind(gauss_distribution, ref(generator));
	std::uniform_real_distribution<double> distribution(0.,1.);
	auto Mersenne = bind(distribution, ref(generator));

	Particle *array = new Particle[N];

//	array[0] = Particle(4,
//			    5,
//			    0,
//			    0,
//			    0,
//			    0,
//			    1,
//			    &array[1]);
//	array[1] = Particle(4 + l-1,
//			    5,
//			    0,
//			    0,
//			    0,
//			    0,
//			    1,
//			    &array[0]);
	int Nroot = sqrt(N / 2);
	int counter = 0;
	for (int i = 0; i < Nroot; i++)
	{
	    for (int j = 0; j < Nroot; j++)
	    {
	    	double pos_x = 20*i + 10;
	    	double pos_y = 20*j + 10;
	    	double phi = Mersenne();
		array[counter] = Particle(pos_x,
					  pos_y,
					  0,
					  sqrt(T) * Gauss(),
					  sqrt(T) * Gauss(),
					  0,
					  1,
					  alpha,
					  Mersenne(),
					  Mersenne(),
					  &array[counter + 1]);
	    	counter++;
	    	array[counter] = Particle(pos_x + l * cos(phi),
	    		                  pos_y + l * sin(phi),
	    		                  0,
					  sqrt(T) * Gauss(),
					  sqrt(T) * Gauss(),
					  0,
					  1,
					  0,
					  Mersenne(),
					  Mersenne(),
					  &array[counter - 1]);
	    	counter++;
	    }
	}
	return array;
}

int main()
{
    // Anfangsparameter
    int NP = 32; // Teilchenanzahl
    double T = 1; // Anfangstemperatur
    int t = 10000; // #Zeitschritte
    double timeStep = .01; // Zeitschrittdifferenz
    tuple <double> size = {80, 80, 5}; // Abmessungen
    double C = 3.; // Cut-off
    double l = 3.5;
    double k = 5000;
    double alpha = 0.01;

    System box(size, NP, createparticles(NP, size, T, l, alpha)); // Erstellung des Systems
    auto LJForce = [](double *f, double x, double y, double z)
    {
    	double r = sqrt(x * x + y * y + z * z);
	f[0] = 24 * (2 * pow(1 / r, 14) - pow(1 / r, 8)) * x;
	f[1] = 24 * (2 * pow(1 / r, 14) - pow(1 / r, 8)) * y;
	f[2] = 24 * (2 * pow(1 / r, 14) - pow(1 / r, 8)) * z;
    };
    auto springField = [k, l](double *f, double x, double y, double z)
    {
    	double r = sqrt(x * x + y * y + z * z);
    	f[0] = - k * (r - l) / r * x;
    	f[1] = - k * (r - l) / r * y;
    	f[2] = - k * (r - l) / r * z;
    };
    auto LJpotential = [](double r)
    {
	return 4 * (pow(1 / r, 12) - pow(1 / r, 6));
    };
    auto springPotential = [k, l](double r)
    {
    	return k / 2 * (r - l) * (r - l);
    };

    // Arrays für die Ausgabe
    double ***velocity = new double**[t]();
    double ***position = new double**[t]();
    double **P = new double*[t]();
    double *Ekin = new double[t]();
    double *Epot = new double[t]();
    for (int i = 0; i < t; i++)
    {
	    velocity[i] = new double*[NP]();
	    position[i] = new double*[NP]();
	    P[i] = new double[3]();
	    for (int j = 0; j < NP; j++)
	    {
		    velocity[i][j] = new double[3]();
		    position[i][j] = new double[3]();
	    }
    }

    // Run the MD-Simulation
    box.calcForces(LJForce, springField, C);

    for (int i = 0; i < t; i++)
    {
    	box.Verlet(LJForce, springField, timeStep, C, velocity[i], position[i]);
    	Ekin[i] = box.kinE();
    	Epot[i] = box.Epot(LJpotential, springPotential, C);
    }

    printPosVel(t, NP, velocity, position);
    printE(t, Ekin, Epot);

    return 0;
}
