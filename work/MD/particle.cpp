#include <cmath>
#include <cstdio>

#include "particle.h"

double Particle::GetPos(char pos)
{
	switch(pos)
	{
		case 'x': return _x;
		case 'y': return _y;
		case 'z': return _z;
		default: return 0;
	}
}

double Particle::GetVel(char pos)
{
	switch(pos)
	{
		case 'x': return _vx;
		case 'y': return _vy;
		case 'z': return _vz;
		default: return  0;
	}
}

void Particle::x_border(double xleft, double xright) //xright > x > xleft
{
	double l = fabs(xleft - xright);
	_x -= l * floor(_x / l);
}

void Particle::y_border(double ydown, double yup)
{
	double l = fabs(ydown - yup);
	_y -= l * floor(_y / l);
}

void Particle::z_border(double zdown, double zup)
{
	double l = fabs(zdown - zup);
	_z -= l * floor(_z / l);
}

void Particle::updatePos(tuple<double> size, double timeStep, double *f)
{
    _x += _vx * timeStep + 1. / (2 * _mass) * f[0] * timeStep * timeStep;
    x_border(0, size.x);
    _y += _vy * timeStep + 1. / (2 * _mass) * f[1] * timeStep * timeStep;
    y_border(0, size.y);
    _z += _vz * timeStep + 1. / (2 * _mass) * f[2] * timeStep * timeStep;
    z_border(0, size.z);
}

void Particle::updateVel(double timeStep, double *oldf, double *newf)
{
    _vx += 1./ (2 * _mass) * (newf[0] + oldf[0]) * timeStep;
    _vy += 1./ (2 * _mass) * (newf[1] + oldf[1]) * timeStep;
    _vz += 1./ (2 * _mass) * (newf[2] + oldf[2]) * timeStep;
}
