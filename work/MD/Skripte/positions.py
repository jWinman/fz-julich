import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

#for t in range(100):
#    fig = plt.figure()
#    ax = fig.add_subplot(1, 1, 1)
#    x, y, z = np.loadtxt("Daten/position{}.csv".format(t), unpack=True)
#    ax.plot(x, y, "ro")
#    fig.savefig("Plots/position{}.pdf".format(t))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_xlim(0, 80)
ax.set_ylim(0, 80)
line, = ax.plot([], [], "ro")

def init():
    line.set_data([], [])
    return line,

def animation(i):
    x, y, z = np.loadtxt("Daten/position{}.csv".format(i), unpack=True)
    line.set_data(x, y)
    return line,

anim = mpl.animation.FuncAnimation(fig, animation, init_func=init, frames=10000)
anim.save("Plots/animation.mp4", fps=200)

