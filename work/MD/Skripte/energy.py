import matplotlib.pyplot as plt
import numpy as np

t, Ekin, Epot, Eges = np.loadtxt("Daten/E.csv", unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, Ekin, label="$E_{\mathrm{kin}}$")
ax.plot(t, Epot, label="$E_{\mathrm{pot}}$")
ax.plot(t, Eges, label="$E_{\mathrm{ges}}$")
ax.set_ylabel("$E$")
ax.set_xlabel("$t$")
ax.legend(loc="best")
fig.savefig("Plots/Energies.pdf")
