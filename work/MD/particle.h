#include "tuple.h"

#ifndef PARTICLE_H
#define PARTICLE_H
class Particle
{
    private:
    	double _x, _y, _z;
    	double _vx, _vy, _vz;
    	double _mass;
    	double _alpha;
    	double _theta;
    	double _phi;
    	Particle *_dumbbell;
    public:
    	Particle() {};
    	Particle(double x, double y, double z,
    		 double vx, double vy, double vz,
    		 double mass, double alpha, double theta, double phi,
    		 Particle *dumbbell) :
    	    _x(x),
    	    _y(y),
    	    _z(z),
	    _vx(vx),
	    _vy(vy),
	    _vz(vz),
    	    _mass(mass),
	    _alpha(alpha),
	    _theta(theta),
	    _phi(phi),
	    _dumbbell(dumbbell){}
	double GetPos(char pos);
	double GetVel(char pos);
	double GetMass() {return _mass;};
	double GetAlpha() {return _alpha;};
	double GetTheta() {return _theta;};
	double GetPhi() {return _phi;};
	Particle *GetDumbbell() {return _dumbbell;};
	void SetTheta(double theta) {_theta += theta;};
	void SetPhi(double phi) {_phi += phi;};
	void x_border(double xleft, double xright);
	void y_border(double ydown, double yup);
	void z_border(double zdown, double zup);
	void updatePos(tuple<double> size, double timeStep, double *f);
	void updateVel(double timeStep, double *oldf, double *newf);

};

#endif
