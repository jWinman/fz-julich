#include <functional>

#include "particle.h"
#include "tuple.h"

#ifndef SYSTEM_H
#define SYSTEM_H
class System
{
    private:
    	tuple<double> _size;
    	int _NP;
    	Particle **_MDparticles;
    	double ***_forces;

    public:
    	System(tuple<double> size, int NP, Particle *MDparticles);
    	void calcForces(std::function<void(double*, double, double, double)> LJfield,
    		        std::function<void(double*, double, double, double)> springField,
    		        double C);
    	void difference(double *diff, Particle *one, Particle *two, tuple<double> size);
    	void Verlet(std::function<void(double*, double, double, double)> LJfield,
    		    std::function<void(double*, double, double, double)> springField,
    		    double timeStep, double C, double **velocity, double **position);
	double kinE();
	double Epot(std::function<double(double)> LJpotential,
		    std::function<double(double)> springPotential,
		    double C);
	void totalMomentum(double *P);
	~System();
};
#endif
