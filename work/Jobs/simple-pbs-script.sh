#!/bin/bash
#
# Carsten Raas <carsten.raas@tu-dortmund.de> / 2012-10-31

#------------------------------------------------------------------------------
# 1. The walltime estimate is absolutely necessary
#    for the cluster to work properly!
# 2. The nodes flag default is nodes=1:ppn=1,
#    so you have to change if you want more cores or/and nodes. 
# 3. The vmem flag default is 100mb,
#    so you have to change if you need more memory.
#------------------------------------------------------------------------------
# Note: "#PBS" is a queuing option, "##PBS" is a comment.
#------------------------------------------------------------------------------

#---------- Job name
#PBS -N simple-pbs-script

#--------- no mail at all:
##PBS -m n
#--------- Mail adress: Don't dare to use a wrong mail adress here. Two cakes!
#PBS -M jens.winkelmann@tu-dortmund.de
#--------- mail for abort, begin, end :
#PBS -m abe
#--------- join STDOUT and STDERR
#PBS -j oe

#--------- estimated (maximum) runtime (default: 1 hour)
#PBS -l walltime=0:01:00
#--------- [[hours:]minutes:]seconds[.milliseconds]
#--------- MUST not be smaller than the real runtime!

#---------- 1 core on one node (default: 1 core on 1 node)
#PBS -l nodes=1:ppn=1

#---------- Maximum amount of total virtual memory (default: 100 MB)
#PBS -l vmem=100mb

module add gcc/4.8.1-full

 cd ${PBS_O_WORKDIR}
 echo "${PBS_JOBNAME}"
 echo
 echo "      PBS_JOBID=${PBS_JOBID}"
 echo "    PBS_ARRAYID=${PBS_ARRAYID}"
 echo "PBS_ENVIRONMENT=${PBS_ENVIRONMENT}"
 echo "   PBS_NODEFILE=${PBS_NODEFILE}"
 echo "     PBS_SERVER=${PBS_SERVER}"
 echo
 if [[ -r ${PBS_NODEFILE} ]]; then cat ${PBS_NODEFILE}; fi
 echo
 env|sort
 echo
 date
 echo "--- START ---"
 date
 pwd
 ls -la /data
 echo -e "scale=200\na(2)\n" |bc -l
 echo "--- END ---"
 date
 echo
 echo "$(whoami) is leaving from $(hostname) ..."
 echo
