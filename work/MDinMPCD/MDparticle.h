#include "tuple.h"

#ifndef MDPARTICLE_H
#define MDPARTICLE_H
class MDparticle
{
    private:
    	double _x, _y, _z;
    	double _vx, _vy, _vz;
    	double _mass;
    	MDparticle *_dumbbell;
    	double _diff[3];
    	double _count[3];
    public:
    	MDparticle() {};
    	MDparticle(double x, double y, double z,
    		 double vx, double vy, double vz,
    		 double mass, MDparticle *dumbbell) :
    	    _x(x),
    	    _y(y),
    	    _z(z),
	    _vx(vx),
	    _vy(vy),
	    _vz(vz),
    	    _mass(mass),
	    _dumbbell(dumbbell),
	    _count{0, 0, 0}{}
	double GetPos(char pos);
	double GetVel(char pos);
	double GetMass() {return _mass;};
	void GetDiff(double diff[]);
	void SetPos(char pos, double value);
	void SetVel(char pos, double value);
	void SetDiff(double diff[]);
	void GetCount(double count[]);
	void SetCountZero();
	MDparticle *GetDumbbell() {return _dumbbell;};
	void x_border(double xleft, double xright);
	void y_border(double ydown, double yup);
	void z_border(double zdown, double zup);
	void updatePos(tuple<double> size, double timeStep, double *f);
	void updateVel(double timeStep, double *oldf, double *newf);
	~MDparticle() {};

};

#endif
