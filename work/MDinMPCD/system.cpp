#include<cmath>
#include <functional>

#include "MDparticle.h"
#include "system.h"
#include "tuple.h"

// Konstruktor: Erstellt System
// - tuple<double> _size :: Größe des Systems,
// - double _T :: Temperatur des Systems,
// - double _NP :: Anzahl der Teilchen,
// - MDparticle *_MDparticles :: Array mit allen Teilchen,
// - double *** _forces :: Kräfte des jetzigen und nächsten Zeitschritts.

double norm(double diff[])
{
    return sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]);
}

System::System(tuple<double> size, int NP, MDparticle *MDParticles) :
    _size(size),
    _NP(NP)
{
    // Speicher für teilchenarray anfordern
    // Pointer auf Teilchen
    _MDparticles = new MDparticle*[_NP]();
    for (int i = 0; i < _NP; i++)
    {
    	_MDparticles[i] = &MDParticles[i];
    }
    _forces = new double**[_NP]();
    for (int i = 0; i < _NP; i++)
    {
    	_forces[i] = new double*[2]();
    	_forces[i][0] = new double[3]();
    	_forces[i][1] = new double[3]();
    }
}

// Verlet() :: führt EINEN Verlet-zeitschritt aus
void System::Verlet(std::function<void(double*, double, double, double)> LJfield,
	            std::function<void(double*, double, double, double)> springField,
	            double timeStep, double C, double **velocity, double **position)
{
    for (int i = 0; i < _NP; i++)
    {
    	_MDparticles[i]->updatePos(_size, timeStep, _forces[i][1]);
    	position[i][0] = _MDparticles[i]->GetPos('x');
    	position[i][1] = _MDparticles[i]->GetPos('y');
    	position[i][2] = _MDparticles[i]->GetPos('z');
    }
    calcForces(LJfield, springField, C);
    for (int i = 0; i < _NP; i++)
    {
    	_MDparticles[i]->updateVel(timeStep, _forces[i][0], _forces[i][1]);
    	velocity[i][0] = _MDparticles[i]->GetVel('x');
    	velocity[i][1] = _MDparticles[i]->GetVel('y');
    	velocity[i][2] = _MDparticles[i]->GetVel('z');
    }
}

// calcForces() :: berechnet neue Kräfte
// LJ-Kraft für alle Teilchen mit Cut-Off C
// Federkraft zwischen Paar von Teilchen
void System::calcForces(std::function<void(double*, double, double, double)> LJfield,
	                std::function<void(double*, double, double, double)> springField,
	                double C)
{
    for (int i = 0; i < _NP; i++)
    {
    	// neue Kräfte werden alte Kräfte
	_forces[i][0][0] = _forces[i][1][0];
	_forces[i][0][1] = _forces[i][1][1];
	_forces[i][0][2] = _forces[i][1][2];
	// neue Kräfte = 0 setzen
	_forces[i][1][0] = 0;
	_forces[i][1][1] = 0;
	_forces[i][1][2] = 0;
    }
    // Differenz und neue Kräfte zwischen Teilchen i und j
    double diff[3];
    double *LJ = new double[3]();
    double *spring = new double[3]();

    for (int i = 0; i < _NP; i++)
    {
    	for (int j = 0; j < i; j++)
    	{
    	    difference(diff, _MDparticles[i], _MDparticles[j], _size);

	    // Federkräfte berechnen
	    if (_MDparticles[i]->GetDumbbell() == _MDparticles[j])
	    {
		springField(spring, diff[0], diff[1], diff[2]);
		_MDparticles[i]->SetDiff(diff);
		_MDparticles[j]->SetDiff(diff);
	    }
	    else
	    {
	    	spring[0] = 0;
	    	spring[1] = 0;
	    	spring[2] = 0;
	    }

	    // LJ-Kräfte berechnen
	    if (norm(diff) < C)
	    {
		LJfield(LJ, diff[0], diff[1], diff[2]);
	    }
	    else
	    {
	    	LJ[0] = 0;
	    	LJ[1] = 0;
	    	LJ[2] = 0;
	    }

    	    // neue Kräfte durch Subtraktion/Addition
	    _forces[i][1][0] -= LJ[0] + spring[0];
	    _forces[i][1][1] -= LJ[1] + spring[1];
	    _forces[i][1][2] -= LJ[2] + spring[2];
	    _forces[j][1][0] += LJ[0] + spring[0];
	    _forces[j][1][1] += LJ[1] + spring[1];
	    _forces[j][1][2] += LJ[2] + spring[2];
    	}
    }
    delete[] LJ;
    delete[] spring;
}

// difference() :: berechnet differenz zwischen zwei Teilchen unter berücksichtigung der period. RB
void System::difference(double diff[], MDparticle *one, MDparticle *two, tuple<double> size)
{
    auto xborder = [size](double x)
    {
    	double L = size.x;
    	return x -  L * floor(x / L + 0.5);
    };
    auto yborder = [size](double y)
    {
    	double L = size.y;
    	return y -  L * floor(y / L + 0.5);
    };
    auto zborder = [size](double z)
    {
    	double L = size.z;
    	return z -  L * floor(z / L + 0.5);
    };
    diff[0] = xborder(two->GetPos('x') - one->GetPos('x'));
    diff[1] = yborder(two->GetPos('y') - one->GetPos('y'));
    diff[2] = zborder(two->GetPos('z') - one->GetPos('z'));
}

// Messung von Observablen

double System::Epot(std::function<double(double)> LJpotential,
		    std::function<double(double)> springPotential,
		    double C)
{
    double Epot = 0;
    for (int i = 0; i < _NP; i++)
    {
    	for (int j = 0; j < i; j++)
    	{
    	    double *diff = new double[3]();

    	    difference(diff, _MDparticles[i], _MDparticles[j], _size);
	    double d = norm(diff);
	    if (_MDparticles[i]->GetDumbbell() == _MDparticles[j])
	    {
	    	Epot += springPotential(d);
	    }
    	    if (d > C) continue;
    	    Epot += LJpotential(d);
    	}
    }
    return Epot;
}

double System::kinE()
{
    double kinE = 0;
    for (int i = 0; i < _NP; i++)
    {
    	double vx = _MDparticles[i]->GetVel('x'),
	       vy = _MDparticles[i]->GetVel('y'),
	       vz = _MDparticles[i]->GetVel('z');
    	kinE += _MDparticles[i]->GetMass() * (vx * vx + vy * vy + vz * vz);
    }
    return 0.5 * kinE;
}

void System::SetMDparticle(int i, char pos, double value)
{
    _MDparticles[i]->SetVel(pos, value);
}

// DEKONSTRUKTOR
System::~System()
{
    for (int i = 0; i < _NP; i++)
    {
	delete[] _forces[i][0];
	delete[] _forces[i][1];
	delete[] _forces[i];
    }
    delete[] _forces;
    delete[] _MDparticles;
}
