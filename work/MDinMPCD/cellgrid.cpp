#include <cmath>
#include <iostream>
#include <functional>
#include <random>

#include <omp.h>

#include "cellgrid.h"
#include "particle.h"
#include "tuple.h"

std::random_device rd;
std::mt19937 generator(rd());

// ---------------------- Konstruktor ------------------------------------------------------

CellGrid::CellGrid(tuple<double> Nxyz, int a, int N_Particle) :
    _a(a),
    _N_Particle(N_Particle)
{
	_Nx = Nxyz.x,
	_Ny = Nxyz.y,
	_Nz = Nxyz.z;

	// Speicher anfordern
	_particle_array = new Particle****[_Nx]();
	_u = new double***[_Nx]();
	_R = new double***[_Nx]();
	_Particle_per_cell = new int**[_Nx]();
	for (int i = 0; i < _Nx; i++)
	{
		_particle_array[i] = new Particle***[_Ny]();
		_u[i] = new double**[_Ny]();
		_R[i] = new double**[_Ny]();
		_Particle_per_cell[i] = new int*[_Ny]();
		for (int j = 0; j < _Ny; j++)
		{
			_particle_array[i][j] = new Particle**[_Nz]();
			_u[i][j] = new double*[_Nz]();
			_R[i][j] = new double*[_Nz]();
			_Particle_per_cell[i][j] = new int[_Nz]();
			
			for (int l = 0; l < _Nz; l++)
			{
			_particle_array[i][j][l] = new Particle*[_N_Particle]();
			_u[i][j][l] = new double[3]();
			_R[i][j][l] = new double[3]();
			_u[i][j][l][0] = 0;
			_u[i][j][l][1] = 0;
			_u[i][j][l][2] = 0;
			}
		}
	}
}

// ---------------------- Ghost-particles --------------------------------------------------

void CellGrid::ghost_particles(int i, int j, int l, double T, double M)
{
	using namespace std;
	double sigma = sqrt((M - _Particle_per_cell[i][j][l]) * T);
	normal_distribution<double> gauss_distribution(0, sigma);
	auto Gauss = bind(gauss_distribution, ref(generator));
	_u[i][j][l][0] = (_u[i][j][l][0] + Gauss()) / M;
	_u[i][j][l][1] = (_u[i][j][l][1] + Gauss()) / M;
	_u[i][j][l][2] = (_u[i][j][l][2] + Gauss()) / M;
}

// ---------------------- Reset-Funktionen --------------------------------------------------

void CellGrid::reset_all()
{
	std::uniform_real_distribution<double> distribution(0.,1.);
	std::uniform_real_distribution<double> distribution2(-1.,1.);
	auto Mersenne = bind(distribution, ref(generator));
	auto Mersenne2 = bind(distribution2, ref(generator));
	// Nullsetzen und Startwerte setzen
	_shift = {_a / 2. - Mersenne(), _a / 2. - Mersenne(), _a / 2. - Mersenne()};
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
			for (int l = 0; l < _Nz; l++) // Schleife über z-Zellen
			{
			    double theta = Mersenne2();
			    double phi = Mersenne();
			    _Particle_per_cell[i][j][l] = 0;
			    _R[i][j][l][0] = sqrt(1 - theta * theta) * cos(2 * M_PI * phi);
			    _R[i][j][l][1] = sqrt(1 - theta * theta) * sin(2 * M_PI * phi);
			    _R[i][j][l][2] = theta;
			}
		}
	}
}

void CellGrid::reset_particle_array()
{
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
			for (int l = 0; l < _Nz; l++) // Schleife über z-Zellen
			{
			_Particle_per_cell[i][j][l] = 0;
			}
		}
	}
}

// ---------------------- Zellen-Auffüllen --------------------------------------------------

void CellGrid::fill_up_particle_array(Particle *array, int length, tuple<double> size)
{
	for (int k = 0; k < length; k++) // Schleife über alle Teilchen
	{
		// Auffüllen in _particle_array unter Beachtung des shifts
		array[k].shift(_shift, 0, size.x, 0, size.z, 0, size.y); // shiften
		int tmpx = array[k].GetPos('x') / _a,
		    tmpy = array[k].GetPos('y') / _a,
		    tmpz = array[k].GetPos('z') / _a;

		_particle_array[tmpx][tmpy][tmpz][_Particle_per_cell[tmpx][tmpy][tmpz]] = &array[k];
		_Particle_per_cell[tmpx][tmpy][tmpz]++;
		array[k].shift(inverse(_shift), 0, size.x, 0, size.z, 0, size.y); //zurückshiften
	}
}

void CellGrid::acceleration_cell(Particle *array, int length, double time, double force)
{
	for (int k = 0; k < length; k++)
	{
		array[k].acceleration(time, force); // alle Teilchen beschleunigen
	}
}

double CellGrid::isotherm_thermostate(int i, int j, int l, double T)
{
	std::gamma_distribution<double> distribution3(3 / 2 * (_Particle_per_cell[i][j][l] - 1), T);
	auto Gamma = bind(distribution3, ref(generator));

	double kinEnergy = 0;
	for (int k = 0; k < _Particle_per_cell[i][j][l]; k++) // Schleife über Teilchen in der Zelle (i, j, l)
	{
	    double delta_vx = _particle_array[i][j][l][k]->Getv('x') - _u[i][j][l][0],
		   delta_vy = _particle_array[i][j][l][k]->Getv('y') - _u[i][j][l][1],
		   delta_vz = _particle_array[i][j][l][k]->Getv('z') - _u[i][j][l][2];
	    double mass = _particle_array[i][j][l][k]->Getmass();
	    kinEnergy += 0.5 * mass * (delta_vx * delta_vx
	    	                     + delta_vy * delta_vy
	    	                     + delta_vz * delta_vz);
	}
	if (kinEnergy == 0) return 1;
	else return sqrt(2 * Gamma() / (2 * kinEnergy));
}

double CellGrid::isokin_thermostate(int i, int j, int l, double T)
{
	double kinEnergy = 0;
	for (int k = 0; k < _Particle_per_cell[i][j][l]; k++) // Schleife über Teilchen in der Zelle (i, j, l)
	{
	    double delta_vx = _particle_array[i][j][l][k]->Getv('x') - _u[i][j][l][0],
		   delta_vy = _particle_array[i][j][l][k]->Getv('y') - _u[i][j][l][1],
		   delta_vz = _particle_array[i][j][l][k]->Getv('z') - _u[i][j][l][2];
	    double mass = _particle_array[i][j][l][k]->Getmass();
	    kinEnergy += 0.5 * mass * (delta_vx * delta_vx
	    	                     + delta_vy * delta_vy
	    	                     + delta_vz * delta_vz);
	}
	if (kinEnergy == 0) return 1;
	else return sqrt(3 * (_Particle_per_cell[i][j][l] - 1) * T / (2 * kinEnergy));
}

void CellGrid::collision(tuple<double> size, double time, double M, double T)
{
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
		    for (int l = 0; l < _Nz; l++) // Schleife über y-Zellen
		    {
			_u[i][j][l][0] = 0;
			_u[i][j][l][1] = 0;
			_u[i][j][l][2] = 0;
			double cellMass = 0;
			for (int k = 0; k < _Particle_per_cell[i][j][l]; k++) // Schleife über Teilchen in der Zelle (i, j, l)
			{
				double particleMass = _particle_array[i][j][l][k]->Getmass();
				_u[i][j][l][0] += particleMass * _particle_array[i][j][l][k]->Getv('x');
				_u[i][j][l][1] += particleMass * _particle_array[i][j][l][k]->Getv('y');
				_u[i][j][l][2] += particleMass * _particle_array[i][j][l][k]->Getv('z');
				cellMass += particleMass;
			}
			if (cellMass != 0)
			{
			    _u[i][j][l][0] /= cellMass;
			    _u[i][j][l][1] /= cellMass;
			    _u[i][j][l][2] /= cellMass;
			}

			double kappa = isokin_thermostate(i, j, l, T);

			for (int k = 0; k < _Particle_per_cell[i][j][l]; k++)
			{
				_particle_array[i][j][l][k]->collisionSRD3D(_u[i][j][l], _R[i][j][l], 2. * M_PI * 130. / 360., kappa);
			}
		    }
		}
	}
}

void CellGrid::propulsion(double M, double propulsion)
{
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
		    for (int l = 0; l < _Nz; l++) // Schleife über y-Zellen
		    {
			bool propOn = false;
			double diff[3];
			for (int k = 0; k < _Particle_per_cell[i][j][l]; k++) // Schleife über Teilchen in der Zelle (i, j, l)
			{
				double particleMass = _particle_array[i][j][l][k]->Getmass();
				if (particleMass == M)
				{
				    propOn = _particle_array[i][j][l][k]->GetProp();
			    	    _particle_array[i][j][l][k]->GetDiff(diff);
				}
			}

			for (int k = 0; k < _Particle_per_cell[i][j][l]; k++)
			{
				if (propOn)
				{
				    _particle_array[i][j][l][k]->prop(propulsion, _Particle_per_cell[i][j][l] - 1, diff);
				}
			}
		    }
		}
	}
}

void CellGrid::streaming(tuple<double> size, double time)
{
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
		    for (int l = 0; l < _Nz; l++) // Schleife über y-Zellen
		    {
			for (int k = 0; k < _Particle_per_cell[i][j][l]; k++)
			{
				_particle_array[i][j][l][k]->streaming(time);
				_particle_array[i][j][l][k]->y_border(0., size.y);
				_particle_array[i][j][l][k]->x_border(0., size.x);
				_particle_array[i][j][l][k]->z_border(0., size.z);
			}
		    }
		}
	}
}

// ---------------------- kinEnergie --------------------------------------------------------

double CellGrid::kinE(Particle *array, int NP)
{
    double kinE = 0;
    for (int i = 0; i < _N_Particle; i++)
    {
    	double vx = array[i].Getv('x'),
	       vy = array[i].Getv('y'),
    	       vz = array[i].Getv('z');
    	kinE += array[i].Getmass() * (vx * vx + vy * vy + vz * vz);
    }
    return 0.5 * kinE;
}

double CellGrid::momentum(Particle *array, int NP)
{
    double momentumx = 0;
    double momentumy = 0;
    double momentumz = 0;
    for (int i = 0; i < _N_Particle; i++)
    {
    	momentumx += array[i].Getmass() * array[i].Getv('x'),
	momentumy += array[i].Getmass() * array[i].Getv('y'),
    	momentumz += array[i].Getmass() * array[i].Getv('z');
    }
    return momentumx;
}

// ---------------------- DESTRUKTOR --------------------------------------------------------

CellGrid::~CellGrid()
{
	for (int i = 0; i < _Nx; i++)
	{
		for (int j = 0; j < _Ny; j++)
		{
			for (int l = 0; l < _Nz; l++)
			{
			    delete[] _particle_array[i][j][l];
			    delete[] _u[i][j][l];
			}
			delete[] _particle_array[i][j];
			delete[] _u[i][j];
			delete[] _Particle_per_cell[i][j];
			delete[] _R[i][j];
		}
		delete[] _particle_array[i];
		delete[] _u[i];
		delete[] _Particle_per_cell[i];
		delete[] _R[i];
	}
	delete[] _particle_array;
	delete[] _u;
	delete[] _Particle_per_cell;
	delete[] _R;
}
