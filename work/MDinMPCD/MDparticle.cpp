#include <cmath>
#include <cstdio>

#include "MDparticle.h"

double MDparticle::GetPos(char pos)
{
	switch(pos)
	{
		case 'x': return _x;
		case 'y': return _y;
		case 'z': return _z;
		default: return 0;
	}
}

double MDparticle::GetVel(char pos)
{
	switch(pos)
	{
		case 'x': return _vx;
		case 'y': return _vy;
		case 'z': return _vz;
		default: return  0;
	}
}

void MDparticle::SetPos(char pos, double value)
{
	switch(pos)
	{
		case 'x': _x = value; break;
		case 'y': _y = value; break;
		case 'z': _z = value; break;
	}
}

void MDparticle::SetVel(char pos, double value)
{
	switch(pos)
	{
		case 'x': _vx = value; break;
		case 'y': _vy = value; break;
		case 'z': _vz = value; break;
	}
}

void MDparticle::SetDiff(double diff[])
{
    _diff[0] = diff[0];
    _diff[1] = diff[1];
    _diff[2] = diff[2];
}

void MDparticle::GetDiff(double diff[])
{
    diff[0] = _diff[0];
    diff[1] = _diff[1];
    diff[2] = _diff[2];
}

void MDparticle::GetCount(double count[])
{
    count[0] = _count[0];
    count[1] = _count[1];
    count[2] = _count[2];
}

void MDparticle::SetCountZero()
{
    _count[0] = 0;
    _count[1] = 0;
    _count[2] = 0;
}

void MDparticle::x_border(double xleft, double xright) //xright > x > xleft
{
	double l = fabs(xleft - xright);
	_count[0] += floor(_x / l);
	_x -= l * floor(_x / l);
}

void MDparticle::y_border(double ydown, double yup)
{
	double l = fabs(ydown - yup);
	_count[1] += floor(_y / l);
	_y -= l * floor(_y / l);
}

void MDparticle::z_border(double zdown, double zup)
{
	double l = fabs(zdown - zup);
	_count[2] += floor(_z / l);
	_z -= l * floor(_z / l);
}

void MDparticle::updatePos(tuple<double> size, double timeStep, double *f)
{
    _x += _vx * timeStep + 1. / (2 * _mass) * f[0] * timeStep * timeStep;
    x_border(0, size.x);
    _y += _vy * timeStep + 1. / (2 * _mass) * f[1] * timeStep * timeStep;
    y_border(0, size.y);
    _z += _vz * timeStep + 1. / (2 * _mass) * f[2] * timeStep * timeStep;
    z_border(0, size.z);
}

void MDparticle::updateVel(double timeStep, double *oldf, double *newf)
{
    _vx += 1./ (2 * _mass) * (newf[0] + oldf[0]) * timeStep;
    _vy += 1./ (2 * _mass) * (newf[1] + oldf[1]) * timeStep;
    _vz += 1./ (2 * _mass) * (newf[2] + oldf[2]) * timeStep;
}
