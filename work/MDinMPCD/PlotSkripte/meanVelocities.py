import matplotlib.pyplot as plt
import numpy as np

v0MSDs = np.loadtxt("Daten/v0/v0MSD.csv", unpack=True)
#coins = np.array([0.03, 0.05, 0.07, 0.1, 0.2, 0.3, 0.4, 0.5, 0.7, 1.0])
#coinsStr = ["0.03", "0.05", "0.07", "0.10", "0.20", "0.30", "0.40", "0.50", "0.70", "1.00"]
#Colors = ["r", "b", "k", "c", "m", "orange", "g", "maroon", "violet", "darkgreen"]

coins = np.array([0.03, 0.05, 0.07, 0.1, 0.2, 0.3, 0.4, 0.5, 0.7, 0.8, 0.9, 1.0, 1.2, 1.4, 1.6])
coinsStr = ["0.03", "0.05", "0.07", "0.10", "0.20", "0.30", "0.40", "0.50", "0.70", "0.80", "0.90", "1.00", "1.20", "1.40", "1.60"]
Colors = ["r", "b", "k", "c", "m", "r", "b", "k", "c", "m", "orange", "g", "maroon", "violet", "darkgreen"]


fig1 = plt.figure()
ax1 = fig1.add_subplot(1, 1, 1)
fig2 = plt.figure()
ax2 = fig2.add_subplot(1, 1, 1)

vAarray = np.array([])
vBarray = np.array([])

for coin, coinStr, Color, v0MSD in zip(coins, coinsStr, Colors, v0MSDs):
    t, va = np.loadtxt("Daten/meanVelocities/meanVelocitiesA{}.csv".format(coinStr), unpack=True)
    t, vb = np.loadtxt("Daten/meanVelocities/meanVelocitiesB{}.csv".format(coinStr), unpack=True)
    t *= 0.1

    vAarray = np.append(vAarray, np.mean(va[100:150]))
    vBarray = np.append(vBarray, np.mean(vb[:50]))

    # Plots
    ax1.plot(t, va, color=Color, marker= ".", linestyle="--", label=r"$\alpha = {}$".format(coin))
    ax2.plot(t, vb, color=Color, marker= ".", linestyle="--", label=r"$\alpha = {}$".format(coin))
    ax1.plot(t, np.full(len(t), v0MSD), color=Color, linestyle="--")
    ax2.plot(t, np.full(len(t), v0MSD), color=Color, linestyle="--")

np.savetxt("Daten/v0/vA.csv", vAarray)
np.savetxt("Daten/v0/vB.csv", vBarray)

ax1.set_title(r"$\langle |\vec{v}(\Updelta t)| \rangle = \langle | \frac{\vec{r}(t + \Updelta t) - \vec{r}(t)}{\Updelta t} | \rangle$")
ax1.set_xlabel("$\Delta t$")
ax1.set_ylabel(r"$\langle |\vec{v}(\Updelta t)| \rangle$")
#ax1.set_xlim(0, 30)
ax1.set_ylim(0, 1.2)
ax1.legend(loc="best", ncol=2)

ax2.set_title(r"$\langle \vec{v}(\Delta t) \cdot \vec{e}_{\mathrm{dumb}} \rangle = \langle \frac{\vec{r}(t + \Updelta t) - \vec{r}(t)}{\Updelta t} \cdot \vec{e}_{\mathrm{dumb}} \rangle$")
ax2.set_xlabel("$\Delta t$")
ax2.set_ylabel(r"$\langle \vec{v}(\Delta t) \cdot \vec{e}_{\mathrm{dumb}} \rangle$")
ax2.set_ylim(0, 1.2)
#ax2.set_xlim(0, 5)
ax2.legend(loc="best", ncol=2)
fig1.savefig("Plots/meanVelocitiesA.pdf")
fig2.savefig("Plots/meanVelocitiesB.pdf")
