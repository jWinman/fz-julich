import matplotlib.pyplot as plt
import numpy as np

density = np.loadtxt("Daten/dichte.csv", unpack=True)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_xlabel("$x / a$")
ax.set_ylabel("$y / a$")
im = ax.imshow(density, origin="lower", interpolation="nearest")
cb = fig.colorbar(im, ax = ax)
cb.set_label(r"mittlere Teilchenzahl $M$")
fig.savefig("Plots/dichte.pdf")
