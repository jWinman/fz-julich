import matplotlib.pyplot as plt
import numpy as np
import math
from decimal import Decimal
import scipy.optimize as sco
import uncertainties as un

def roundsig(value, figures=3):
    d = Decimal(value)
    if d == 0:
        return '0'
    d = round(d, figures - int(math.floor(math.log10(abs(d)))) - 1)
    return "{:f}".format(d)

# Arrays für versch. alpha, colors und v0
coins = np.array([0.03, 0.05, 0.07, 0.1, 0.2, 0.3, 0.4, 0.5, 0.7, 0.8, 0.9, 1.0, 1.2, 1.4, 1.6])
coinsStr = ["0.03", "0.05", "0.07", "0.10", "0.20", "0.30", "0.40", "0.50", "0.70", "0.80", "0.90", "1.00", "1.20", "1.40", "1.60"]
colors = ["r", "b", "k", "c", "m", "r", "b", "k", "c", "m", "orange", "g", "maroon", "violet", "darkgreen"]

#coins = np.array([0.03, 0.05, 0.07, 0.1, 0.2, 0.3, 0.4, 0.5, 0.7, 1.0])
#coinsStr = ["0.03", "0.05", "0.07", "0.10", "0.20", "0.30", "0.40", "0.50", "0.70", "1.00"]
#colors = ["r", "b", "k", "c", "m", "orange", "g", "maroon", "violet", "darkgreen"]

v0array = np.array([])

M = 20
tauArray = np.array([])

fig1 = plt.figure()
ax1 = fig1.add_subplot(1, 1, 1)

fig2 = plt.figure()
ax2 = fig2.add_subplot(1, 1, 1)

for coin, coinStr, Color in zip(coins, coinsStr, colors):
#################################### Korrelation ############################################
    t, oKorr = np.loadtxt("Daten/orientationKorr/oKorr{}.csv".format(coinStr), unpack=True)
    t *= 0.1

    # Fit
    korrelation = lambda x, tau: np.exp(-x / tau)
    popt, pcov = sco.curve_fit(korrelation, t[:len(t) // 3], oKorr[:len(t) // 3], p0=50)
    tau = un.ufloat(popt[0], np.sqrt(pcov[0][0]))

    # Plots
    ax1.plot(t, oKorr, color=Color, marker=".", linestyle="", label=r"$\alpha = {}$".format(coin))
    ax1.plot(t, korrelation(t, tau.nominal_value), color=Color, label=r"$\tau_r = {}$".format(round(tau.nominal_value, 2)))

######################################## MSD ###############################################
    t, MSD = np.loadtxt("Daten/MSD/MSD{}.csv".format(coinStr), unpack=True)
    t *= 0.1
    t, MSD = t[:79], MSD[:79]

   # Plots
    ax2.plot(t, MSD, color=Color, marker=".", linestyle="", label=r"$\alpha = {}$".format(coin))

    # Fit
    # ohne Antrieb
    if (coin == 0):
        MSD_fit = lambda t, gamma: 6 / gamma * t - 6 * M / gamma**2 * (1 - np.exp(-gamma * t / M))
        popt, pcov = sco.curve_fit(MSD_fit, t, MSD, p0=100)
        gamma = un.ufloat(popt[0], np.sqrt(pcov[0][0]))
        print(coin, gamma)

        ax2.plot(t, MSD_fit(t, gamma.nominal_value), color=Color, label=r"$\gamma = {}$".format(roundsig(gamma.nominal_value)))

    # mit Antrieb
    else:
        tau = tau.nominal_value
        if (v0array.size == 0):
            v0propose = 0.005
        else:
            v0propose = v0array[-1] + 0.1
        MSD_fit = lambda t, v0, gamma: 6 / gamma * t - 6 * M / gamma**2 * (1 - np.exp(-gamma * t / M)) + 2 * v0**2 * tau**2 * (t / tau + np.exp(-t / tau) - 1)
        popt, pcov = sco.curve_fit(MSD_fit, t, MSD, p0=[v0propose, 100])
        #print(coin, popt, pcov)
        v0 = un.ufloat(popt[0], np.sqrt(pcov[0][0]))
        gamma = un.ufloat(popt[1], np.sqrt(pcov[1][1]))
        v0array = np.append(v0array, v0.nominal_value)
        print(coin, v0, gamma)

        ax2.plot(t, MSD_fit(t, v0.nominal_value, gamma.nominal_value), color=Color, label=r"$v_0 = {}$".format(roundsig(v0.nominal_value)))

ax1.set_xlabel("$t$")
ax1.set_ylabel(r"$\langle \vec{e}(t + t_0) \vec{e}(t_0) \rangle$")
ax1.set_ylim(0, 1.5)
ax1.legend(loc="best", ncol=2)
fig1.savefig("Plots/oKorrcoins.pdf")

ax2.set_xlabel("$t$")
ax2.set_ylabel(r"$\langle (\vec{r}(t + t_0) - \vec{r}(t_0))^2 \rangle$")
ax2.set_ylim(0, 1900)
ax2.legend(loc="best", ncol=2)
fig2.savefig("Plots/MSDcoins.pdf")

np.savetxt("Daten/v0/v0MSD.csv", v0array)
