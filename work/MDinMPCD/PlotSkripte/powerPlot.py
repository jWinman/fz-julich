import math
import matplotlib.pyplot as plt
import numpy as np
import linregress as lin
import scipy.optimize as sco
import uncertainties as un
from decimal import Decimal

def roundsig(value, figures=3):
    d = Decimal(value)
    if d == 0:
        return '0'
    d = round(d, figures - int(math.floor(math.log10(abs(d)))) - 1)
    return "{:f}".format(d)

v0, powerIn, powerOut, powerTot = np.loadtxt("Daten/v0/Power.csv", unpack=True)
coins = np.array([0.03, 0.05, 0.07, 0.1, 0.2, 0.3, 0.4, 0.5, 0.7, 0.8, 0.9, 1.0, 1.2, 1.4, 1.6])

vA = np.loadtxt("Daten/v0/vA.csv", unpack=True)
vB = np.loadtxt("Daten/v0/vB.csv", unpack=True)
v0MSD = np.loadtxt("Daten/v0/v0MSD.csv", unpack=True)

linear = lambda x, m, b:  m * x + b
root = lambda x, a, b: a * x**b
x = np.linspace(0, coins[-1] + 0.1, 1000)

popt, pcov = sco.curve_fit(root, coins, v0)
a = un.ufloat(popt[0], pcov[0][0])
b = un.ufloat(popt[1], pcov[1][1])
print(a, b)

m, m_error, b, b_error = lin.linregress(coins[:5], v0[:5])

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(coins, vB, "kD", label=r"$v_0 = \langle\frac{\vec{r}_{\mathrm{cm}}(t + \Updelta t) - \vec{r}_{\mathrm{cm}}(t)}{\Updelta t} \cdot \vec{e}_{\mathrm{dumb}}\rangle$")
ax.plot(coins, v0, "ro", label=r"$v_0 = \langle \frac{\vec{v}_{1} + \vec{v}_{2}}{2} \cdot \vec{e}_{\mathrm{dumb}} \rangle$")
ax.plot(coins, v0MSD, "gx", label=r"$v_{0, \mathrm{MSD}}$")
#ax.plot(coins, vA, "bo", label=r"$v_0 = \langle\left|\frac{\vec{r}_{\mathrm{cm}}(t + \Delta t) - \vec{r}_{\mathrm{cm}}(t)}{\Delta t}\right|\rangle$")
#ax.plot(x, root(x, a.nominal_value, b.nominal_value), label=r" $v_0 = {} \cdot \alpha^{{{}}}$".format(roundsig(a.nominal_value), roundsig(b.nominal_value)))
ax.plot(x, linear(x, m, b), "--", label=r" $v_0 = {} \cdot \alpha + {}$".format(roundsig(m), roundsig(b)))

ax.set_ylabel(r"$v_0$")
#ax.set_ylabel(r"$\langle P \rangle$")
ax.set_xlabel(r"$\alpha$")
ax.legend(loc="best")

fig.savefig("Plots/powerplot.pdf")

