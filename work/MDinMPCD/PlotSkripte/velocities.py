import matplotlib.pyplot as plt
import numpy as np

def normalize(v):
    norm=np.linalg.norm(v)
    if norm==0:
       return v
    return v/norm

n = 10000
for propulsion in ["On", "Off"]:
    v_mean_dir = np.zeros(n)
    t = np.arange(n)
    vx, vy, vz = np.loadtxt("Daten/Velocities/velocities{}.csv".format(propulsion), unpack=True)
    x_diff, y_diff, z_diff = np.loadtxt("Daten/Diffs/diffs{}.csv".format(propulsion), unpack=True)
    x, y , z = np.loadtxt("Daten/Positions/positions{}.csv".format(propulsion), unpack=True)

    for i in range(n):
        direction = np.array([x_diff[i], y_diff[i], z_diff[i]])
        direction = normalize(direction)

        v_mean = np.array([(vx[2 * i + 1] + vx[2 * i]) / 2, (vy[2 * i + 1] + vy[2 * i]) / 2, (vz[2 * i + 1] + vz[2 * i]) / 2])
        v_mean_dir[i] = np.dot(v_mean, direction)

    print(propulsion, np.mean(v_mean_dir))
    D = np.mean(v_mean_dir)**2 / (2e-2)
    print(propulsion, D)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(t, v_mean_dir)
    ax.set_xlabel("t")
    ax.set_ylabel("v")

    fig.savefig("Plots/velocity{}.pdf".format(propulsion))

