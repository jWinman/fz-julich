import matplotlib.pyplot as plt
import numpy as np

t, deltaEIn, deltaEOut, E = np.loadtxt("Daten/deltaE.csv", unpack=True)

#a = np.array([1, 2, 4, 5, 10, 20, 50, 100])

#fig = plt.figure()
#ax = fig.add_subplot(1, 1, 1)
#
#for n in a:
#    deltaE = np.split(deltaEOut, n)
#    meanE = np.array(list(map(np.mean, deltaE)))
#    ns = np.array([n for _ in range(len(deltaE))])
#    ax.plot(1000 / ns, meanE, "ro")
#
#f = lambda x: 5/ np.sqrt(x)
#x = np.linspace(10, 1000, 1000)
#
#ax.plot(x, f(x), "--")
#ax.set_ylabel(r"$\langle\Delta E\rangle$")
#ax.set_xlabel("$n$")
##ax.set_xlim(0, 12)
#fig.savefig("Plots/deltaEs.pdf")
#quit()

print(np.mean(deltaEIn), np.mean(deltaEOut))
print(np.mean(deltaEIn), np.mean(deltaEOut[:len(deltaEOut) // 2]))
print(np.mean(deltaEIn), np.mean(deltaEOut[len(deltaEOut) // 2:]))
print(np.mean(deltaEOut**2))
print(np.mean(deltaEOut)**2)

number = 100
deltaE = np.array([np.mean(deltaEOut[number * i:number * i+number]) for i in range(len(deltaEOut) // number)])
t_new = range(len(deltaE))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(t_new, deltaE, label="$\Delta E_{\mathrm{In}}$")
fig.savefig("Plots/deltaEmean.pdf")
#ax.plot(t, deltaEIn, label="$\Delta E_{\mathrm{In}}$")
#ax.plot(t, deltaEOut, label="$\Delta E_{\mathrm{Out}}$")
##ax.plot(t, Epot, label="$E_{\mathrm{pot}}$")
##ax.plot(t, Eges, "r--",label="$E_{\mathrm{ges}}$")
#ax.set_ylabel("$E$")
#ax.set_xlabel("$t$")
#ax.legend(loc="best")
#fig.savefig("Plots/DeltaEnergies.pdf")
