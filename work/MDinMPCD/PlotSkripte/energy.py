import matplotlib.pyplot as plt
import numpy as np

t, Ekin, Epot, Eges, EkinTot = np.loadtxt("Daten/E.csv", unpack=True)

print(np.mean(EkinTot), np.mean(Ekin))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, Ekin, label="$E_{\mathrm{kin}}$")
ax.plot(t, EkinTot, label="$E_{\mathrm{kinTot}}$")
#ax.plot(t, Epot, label="$E_{\mathrm{pot}}$")
#ax.plot(t, Eges, "r--",label="$E_{\mathrm{ges}}$")
ax.set_ylabel("$E$")
ax.set_xlabel("$t$")
ax.legend(loc="best")
fig.savefig("Plots/Energies.pdf")
