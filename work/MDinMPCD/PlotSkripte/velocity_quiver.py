import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.optimize as sc
from uncertainties import *

# quiver
for i in range(100):
    x, y, ux, uy, uz = np.loadtxt("Daten/Velocityfields/velocityfield{}.csv".format(i), unpack=True)
    norm = np.sqrt(ux**2 + uy**2)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.quiver(x, y, ux / norm, uy / norm, norm, cmap='spring')
    ax.set_xlabel("$x / a$")
    ax.set_ylabel("$y / a$")
    norm = np.array(np.split(norm, x[-1] + 1)).T
    im = ax.imshow(norm, origin="lower")
    cb = fig.colorbar(im, ax = ax, orientation="horizontal")
    #cb.set_label(r"$v_{c,\mathrm{cm}} / \frac{a}{\updelta t}$")
    fig.savefig("Plots/Velocityfields/quiver{}.pdf".format(i))

