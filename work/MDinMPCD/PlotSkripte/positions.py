import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

x = np.linspace(0, 10, 1000)
f = lambda x, a: a * x/x

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, projection='3d')
ax.set_xlim(0, 10)
ax.set_ylim(0, 10)
ax.set_zlim(0, 10)
ax.plot(f(x, 0), x, "k")
ax.plot(x, f(x, 10), "k")
ax.plot(f(x, 10), x, f(x, 10), "k")
ax.plot(x, f(x, 10), f(x, 10), "k")
ax.plot(f(x, 0), x, f(x, 10), "k")
ax.plot(x, f(x, 0), f(x, 10), "k")
ax.plot(f(x, 0), f(x, 10), x, "k")
ax.plot(f(x, 0), f(x, 0), x, "k")
ax.plot(f(x, 10), f(x, 0), x, "k")

boundaries = lambda x, l: x  - l * np.floor(x / l)

foo = None
x0, y0, z0 = np.loadtxt("Daten/Positions/positions-0.50-0.csv", unpack=True)
x1, y1, z1 = np.loadtxt("Daten/Positions/positions-0.50-1.csv", unpack=True)

x0, y0, z0 = boundaries(x0[:10000], 10), boundaries(y0[:10000], 10), boundaries(z0[:10000], 10)
x1, y1, z1 = boundaries(x1[:10000], 10), boundaries(y1[:10000], 10), boundaries(z1[:10000], 10)

print("hello")

def init():
    global foo
    foo = ax.scatter([], [], [])
    return []

def animation(i):
    global foo
    foo.remove()
    foo = ax.scatter([x0[i], x1[i]], [y0[i], y1[i]], [z0[i], z1[i]])

    return []

anim = mpl.animation.FuncAnimation(fig, animation, init_func=init, frames=10000)
anim.save("Plots/animation.mp4", fps=100)
