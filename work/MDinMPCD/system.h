#include <functional>

#include "MDparticle.h"
#include "tuple.h"

#ifndef SYSTEM_H
#define SYSTEM_H
class System
{
    private:
    	tuple<double> _size;
    	int _NP;
    	MDparticle **_MDparticles;
    	double ***_forces;

    public:
    	System(tuple<double> size, int NP, MDparticle *MDParticles);
    	void calcForces(std::function<void(double*, double, double, double)> LJfield,
    		        std::function<void(double*, double, double, double)> springField,
    		        double C);
    	void difference(double *diff, MDparticle *one, MDparticle *two, tuple<double> size);
    	void Verlet(std::function<void(double*, double, double, double)> LJfield,
    		    std::function<void(double*, double, double, double)> springField,
    		    double timeStep, double C, double **velocity, double **position);
	double kinE();
	double Epot(std::function<double(double)> LJpotential,
		    std::function<double(double)> springPotential,
		    double C);
	void SetMDparticle(int i, char pos, double value);
	~System();
};
#endif
