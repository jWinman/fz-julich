#include<cmath>
#include<functional>
#include<random>

#include "MDparticle.h"
#include "system.h"
#include "particle.h"
#include "cellgrid.h"

std::random_device rd1;
std::mt19937 generator1(rd1());
std::uniform_real_distribution<double> distribution1(0.,1.);
std::normal_distribution<double> normaldistribution(0., 1.);
auto Gauss1 = bind(normaldistribution, ref(generator1));
auto Mersenne1 = bind(distribution1, ref(generator1));

double norm2(double diff[])
{
    return sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]);
}

// ------------ Alle Print-Funktionen ---------------------------------------------------------------------

// Gibt v_cm und Teilchendichte für jede Zelle aus
void printMeanVelocity(tuple<double> size, int numberOfMeas, double *****mean_xyz, double ****particle_dens)
{
	printf("Output: MeanVelocities\n");
	for (int t = 0; t < numberOfMeas; t++)
	{
	    char name1[100];
	    char name2[100];
	    sprintf(name1, "Daten/Velocityfields/velocityfield%d.csv", t);
	    sprintf(name2, "Daten/Dichten/dichte%d.csv", t);
	    FILE *file1 = fopen(name1, "w");
	    FILE *file2 = fopen(name2, "w");
	    for (int i = 0; i < int(size.x); i++)
	    {
		    for (int j = 0; j < int(size.y); j++)
		    {
			    fprintf(file2, "%.2f ", particle_dens[t][i][j][5]);
			    fprintf(file1, "%d %d %.15f %.15f %.15f\n", i, j, mean_xyz[t][i][j][5][0], mean_xyz[t][i][j][5][1], mean_xyz[t][i][j][5][2]);
		    }
		    fprintf(file2, "\n");
	    }
	    fclose(file1);
	    fclose(file2);
	}
}

// Gibt Geschwindigkeitsverteilung zu jedem Zeitschritt aus
void printVelocityDistributions(Particle *teilchenarray, int NP, int w)
{
	printf("Output von %d \n", w);
	char velocity_distribution[100];
//	/char pos_distribution[100];
	sprintf(velocity_distribution, "Daten/Distributions/velocity_distribution-%d.csv", w);
//	sprintf(pos_distribution, "Daten/Distributions/position_distribution-%d.csv", w);
	FILE *velocityDistribution = fopen(velocity_distribution, "w");
//	FILE *posDistribution = fopen(pos_distribution, "w");
	for (int i = 0; i < NP; i++)
	{
		fprintf(velocityDistribution, "%.15f %.15f %.15f \n", teilchenarray[i].Getv('x'), teilchenarray[i].Getv('y'), teilchenarray[i].Getv('z'));
//		fprintf(posDistribution, "%.15f %.15f %.15f \n", teilchenarray[i].GetPos('x'), teilchenarray[i].GetPos('y'), teilchenarray[i].GetPos('z'));
	}
	fclose(velocityDistribution);
//	fclose(posDistribution);
}

// Gibt Position und Geschwindigkeiten der MD-Teilchen aus
void printPosVel(int running_time, int t, tuple<double> size, int NP, double propulsion, double ***velocity, double ***position, double **counts1, double **counts2)
{
	printf("Output: Position and Vel\n");
      	for (int j = 0; j < NP; j++)
      	{
	    char name1[100];
	    char name2[100];
	    sprintf(name1, "Daten/Velocities/velocities-%.2f-%d.csv",  propulsion, j);
	    sprintf(name2, "Daten/Positions/positions-%.2f-%d.csv", propulsion, j);
	    FILE *file1 = fopen(name1, "w");
	    FILE *file2 = fopen(name2, "w");
	    for (int i = 0; i < running_time * t; i += t)
	    {
		fprintf(file1, "%.10f %.10f %.10f\n", velocity[i][j][0], velocity[i][j][1], velocity[i][j][2]);
		if (j == 0)
		{
		    fprintf(file2, "%.10f %.10f %.10f\n", position[i][j][0] + size.x * counts1[i][0],
		    	                                  position[i][j][1] + size.y * counts1[i][1],
		    	                                  position[i][j][2] + size.z * counts1[i][2]);
		}
		else
		{
		    fprintf(file2, "%.10f %.10f %.10f\n", position[i][j][0] + size.x * counts2[i][0],
		    	                                  position[i][j][1] + size.y * counts2[i][1],
		    	                                  position[i][j][2] + size.z * counts2[i][2]);
		}
	    }
	    fclose(file1);
	    fclose(file2);
	}
}

// Gibt alle Energien der MD-Teilchen aus
void printE(int running_time, double *Ekin, double *EkinTot, double *Epot)
{
	printf("Output: Energies\n");
	char nameE[100];
	sprintf(nameE, "Daten/E.csv");
	FILE *file1 = fopen(nameE, "w");
	for (int i = 0; i < running_time; i++)
	{
	    	fprintf(file1, "%d %.15f %.15f %.15f %.15f\n", i, Ekin[i], Epot[i], Ekin[i] + Epot[i], EkinTot[i]);
	}
	fclose(file1);
}

void printdeltaE(int running_time, double propulsion, double *deltaEIn, double *deltaEOut)
{
	printf("Output: deltaEnergies\n");
	char nameE[100];
	sprintf(nameE, "Daten/DeltaE/deltaE-%.2f.csv", propulsion);
	FILE *file1 = fopen(nameE, "w");
	for (int i = 0; i < running_time; i++)
	{
	    	fprintf(file1, "%d %.15f %.15f %.15f\n", i, deltaEIn[i], deltaEOut[i], deltaEOut[i] + deltaEIn[i]);
	}
	fclose(file1);
}

void printMSD(int measure_time, double MPCtimeStep, double *MSD, double propulsion)
{
	printf("Output: MSD\n");
	char nameMSD[100];
	sprintf(nameMSD, "Daten/MSD/MSD%.2f.csv", propulsion);
	FILE *file1 = fopen(nameMSD, "w");
	for (int i = 0; i < measure_time; i++)
	{
	    	fprintf(file1, "%.10f %.10f\n", double(i) * MPCtimeStep, MSD[i]);
	}
	fclose(file1);
}

void printOrientationKorr(int measure_time, double MPCtimeStep, double *orientationKorr, double propulsion)
{
	printf("Output: orientationKorr\n");
	char nameoKorr[100];
	sprintf(nameoKorr, "Daten/orientationKorr/oKorr%.2f.csv", propulsion);
	FILE *file1 = fopen(nameoKorr, "w");
	for (int i = 0; i < measure_time; i++)
	{
	    	fprintf(file1, "%.10f %.10f\n", double(i) * MPCtimeStep, orientationKorr[i]);
	}
	fclose(file1);
}

// ------------ Erstellt MD- und MPCD-Teilchen ---------------------------------------------------------------------
void createparticles(Particle *array, int MPC_N, int MD_N, tuple<double> size, double T, double M)
{
	double vx_cm = 0;
	double vy_cm = 0;
	double vz_cm = 0;
	for (int i = 0; i < MPC_N; i++)
	{
		double vx = sqrt(T) * Gauss1();
		double vy = sqrt(T) * Gauss1();
		double vz = sqrt(T) * Gauss1();
		array[i] = Particle(Mersenne1() * size.x, Mersenne1() * size.y, Mersenne1() * size.z,
			            vx, vy, vz, 1, false);
		vx_cm += vx / MPC_N;
		vy_cm += vy / MPC_N;
		vz_cm += vz / MPC_N;
	}
	for (int i = MPC_N; i < MPC_N + MD_N; i++)
	{
		array[i] = Particle(0, 0, 0, 0, 0, 0, M, i % 2 == 0 ? true : false);
	}
	for (int i = 0; i < MPC_N; i++)
	{
		double vx = array[i].Getv('x');
		double vy = array[i].Getv('y');
		double vz = array[i].Getv('z');
		array[i].Setv('x', vx - vx_cm);
		array[i].Setv('y', vy - vy_cm);
		array[i].Setv('z', vz - vz_cm);
	}

}

void createMDparticles(MDparticle *array, tuple<double> size, double T, double l, double mass)
{
	array[0] = MDparticle(2,
			    5,
			    5,
			    0,
			    0,
			    0,
			    mass,
			    &array[1]);
	array[1] = MDparticle(2 + l,
			    5,
			    5,
			    0,
			    0,
			    0,
			    mass,
			    &array[0]);
}

int main(int argc, char* argv[])
{
// --------- Anfangsparameter ----------------------------------------------------------
    int MD_N = 2; // Teilchenanzahl für MD
    int MPC_N = 10000 - MD_N; // Teilchenanzahl für MPCD
    double T = 1.; // Anfangstemperatur
    int running_time = 110000; // #Zeitschritte insgesamt
    int measure_time = 100;
//    int start = 5000;
//    int end = 10000;
    double MDtimeStep = .005; // Zeitschrittdifferenz für MD
    double MPCtimeStep = 0.1; // Zeitschrittdifferenz für MPC
    int t = MPCtimeStep / MDtimeStep; // Anzahl an Schritte für MD
    tuple <double> size = {10, 10, 10}; // Abmessungen
    double C = 1.5; // Cut-off
    double l = 2; // Länge zwischen Dumbbell
    double k = 1000; // Federkonstante
    double propulsion = std::atof(argv[1]); // Antrieb der Dumbbells


// --------- Arrays für die Ausgabe ----------------------------------------------------
    double ***velocity = new double**[running_time * t]();
    double ***position = new double**[running_time * t]();
    double **counts1 = new double*[running_time * t]();
    double **counts2 = new double*[running_time * t]();
    double *deltaEIn = new double[running_time / measure_time]();
    double *deltaEOut = new double[running_time / measure_time]();
//    double *Ekin = new double[running_time]();
//    double *EkinTot = new double[running_time]();
//    double *Epot = new double[running_time]();
//    double *MSD = new double[running_time - end]();
//    double *oKorr = new double[running_time - end]();
//    double **diffs = new double*[running_time]();
    for (int i = 0; i < running_time * t; i++)
    {
	    counts1[i] = new double[3]();
	    counts2[i] = new double[3]();
	    velocity[i] = new double*[MD_N]();
	    position[i] = new double*[MD_N]();
	    for (int j = 0; j < MD_N; j++)
	    {
		    velocity[i][j] = new double[3]();
		    position[i][j] = new double[3]();
	    }
    }

// --------------------------------------- SIMULATION ----------------------------------------------------
// ------------ Kräfte initialisieren --------------------------------------------------------------------
    auto LJForce = [](double *f, double x, double y, double z)
    {
    	double r = sqrt(x * x + y * y + z * z);
	f[0] = 24 * (2 * pow(1 / r, 14) - pow(1 / r, 8)) * x;
	f[1] = 24 * (2 * pow(1 / r, 14) - pow(1 / r, 8)) * y;
	f[2] = 24 * (2 * pow(1 / r, 14) - pow(1 / r, 8)) * z;
    };
    auto springField = [k, l](double *f, double x, double y, double z)
    {
    	double r = sqrt(x * x + y * y + z * z);
    	f[0] = - k * (r - l) / r * x;
    	f[1] = - k * (r - l) / r * y;
    	f[2] = - k * (r - l) / r * z;
    };
    auto LJpotential = [](double r)
    {
	return 4 * (pow(1 / r, 12) - pow(1 / r, 6));
    };
    auto springPotential = [k, l](double r)
    {
    	return k / 2 * (r - l) * (r - l);
    };

    //mittlere Teilchenzahldichte
    int M = (MPC_N + MD_N) / (size.x * size.y * size.z);

// ------------ System für MD & MPC erstellen ----------------------------------------------------------------------
    MDparticle *MDteilchenarray = new MDparticle[MD_N]();
    createMDparticles(MDteilchenarray, size, T, l, M);
    System box(size, MD_N, MDteilchenarray);

    Particle *teilchenarray = new Particle[MPC_N + MD_N]();
    createparticles(teilchenarray, MPC_N, MD_N, size, T, M);
    CellGrid zellenarray(size, 1, MPC_N + MD_N);

// ------------ Run the simulation -----------------------------------------------------------------------
    box.calcForces(LJForce, springField, C);
    for (int w = 0; w < running_time; w++)
    {
    	double diff[3];
	// MD-Simulation
    	for (int i = w * t; i < (w + 1) * t; i++)
    	{
    	    box.Verlet(LJForce, springField, MDtimeStep, C, velocity[i], position[i]);
	    MDteilchenarray[0].GetCount(counts1[i]);
	    MDteilchenarray[1].GetCount(counts2[i]);
    	}
    	// MD-Geschw. und -Pos. auf MPC übertragen
    	for (int i = 0; i < MD_N; i++)
    	{
    	    teilchenarray[i + MPC_N].SetPos('x', position[(w + 1) * t - 1][i][0]);
    	    teilchenarray[i + MPC_N].SetPos('y', position[(w + 1) * t - 1][i][1]);
    	    teilchenarray[i + MPC_N].SetPos('z', position[(w + 1) * t - 1][i][2]);
    	    teilchenarray[i + MPC_N].Setv('x', velocity[(w + 1) * t - 1][i][0]);
    	    teilchenarray[i + MPC_N].Setv('y', velocity[(w + 1) * t - 1][i][1]);
    	    teilchenarray[i + MPC_N].Setv('z', velocity[(w + 1) * t - 1][i][2]);
    	    MDteilchenarray[i].GetDiff(diff);
    	    teilchenarray[i + MPC_N].SetDiff(diff);
    	}
    	// MPC-Simulation
	zellenarray.reset_all();
	zellenarray.fill_up_particle_array(teilchenarray, MPC_N + MD_N, size);
	double E_before = zellenarray.kinE(teilchenarray, MPC_N + MD_N);
	zellenarray.collision(size, MPCtimeStep, M, T);
	double E_after = zellenarray.kinE(teilchenarray, MPC_N + MD_N);
	zellenarray.propulsion(M, propulsion);
	double E_afterprop = zellenarray.kinE(teilchenarray, MPC_N + MD_N);
	zellenarray.streaming(size, MPCtimeStep);
	
	// MPC-Geschw. auf MD übertragen
	for (int i = 0; i < MD_N; i++)
	{
	    box.SetMDparticle(i, 'x', teilchenarray[i + MPC_N].Getv('x'));
	    box.SetMDparticle(i, 'y', teilchenarray[i + MPC_N].Getv('y'));
	    box.SetMDparticle(i, 'z', teilchenarray[i + MPC_N].Getv('z'));
	}

	// Observablen messen
	deltaEOut[w / measure_time] += (E_before - E_after);
	deltaEIn[w / measure_time] += (E_after - E_afterprop);
//	diffs[w][0] = diff[0];
//	diffs[w][1] = diff[1];
//	diffs[w][2] = diff[2];
//	for (int i = start; i < end; i++)
//	{
//	    if ((w-i) < running_time - end && (w-i) >= 0)
//	    {
////	    	// MSD
//	    	double x = position[w * t][0][0] + size.x * counts1[w * t][0]
//	    		 - (position[i * t][0][0] + size.x * counts1[i * t][0]);
//	    	double y = position[w * t][0][1] + size.y * counts1[w * t][1]
//	    		 - (position[i * t][0][1] + size.y * counts1[i * t][1]);
//	    	double z = position[w * t][0][2] + size.z * counts1[w * t][2]
//	    		 - (position[i * t][0][2] + size.z * counts1[i * t][2]);
//		MSD[w - i] += (pow(x, 2) + pow(y, 2) + pow(z, 2)) / (end - start);
////		// oKorr
//		double diffx = diffs[i][0] * diffs[w][0],
//		       diffy = diffs[i][1] * diffs[w][1],
//		       diffz = diffs[i][2] * diffs[w][2];
//		oKorr[w - i] += (diffx + diffy + diffz) / (norm2(diffs[i]) * norm2(diff) * (end - start));
//	    }
//	}
    }
    delete[] teilchenarray;
    delete[] MDteilchenarray;
   //printMeanVelocity(size, numberOfMeas, mean_xyz, particle_dens);
   printPosVel(running_time, t, size, MD_N, propulsion, velocity, position, counts1, counts2);
   //printE(running_time, Ekin, EkinTot, Epot);
//   printMSD(running_time - end, MPCtimeStep, MSD, propulsion);
//   printOrientationKorr(running_time - end, MPCtimeStep, oKorr, propulsion);
   printdeltaE(running_time, propulsion, deltaEIn, deltaEOut);

   return 0;
}
