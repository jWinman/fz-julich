#include <cmath>
#include <iostream>

#include "particle.h"
#include "tuple.h"

double norm1(double diff[])
{
    return sqrt(diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]);
}

// --------------------- Auslesen der Position des Teilchens ----------------------

double Particle::GetPos(char pos)
{
	switch(pos)
	{
		case 'x': return _x;
		case 'y': return _y;
		case 'z': return _z;
		default: return 0;
	}
}

double Particle::Getv(char pos)
{
	switch(pos)
	{
		case 'x': return _v_x;
		case 'y': return _v_y;
		case 'z': return _v_z;
		default: return  0;
	}
}

// --------------------- Setzen der Position des Teilchens ----------------------

void Particle::SetPos(char pos, double value)
{
	switch(pos)
	{
		case 'x': _x = value; break;
		case 'y': _y = value; break;
		case 'z': _z = value; break;
	}
}

void Particle::Setv(char pos, double value)
{
	switch(pos)
	{
		case 'x': _v_x = value; break;
		case 'y': _v_y = value; break;
		case 'z': _v_z = value; break;
	}
}

void Particle::SetDiff(double diff[])
{
    _diff[0] = diff[0];
    _diff[1] = diff[1];
    _diff[2] = diff[2];
}

void Particle::GetDiff(double diff[])
{
    diff[0] = _diff[0];
    diff[1] = _diff[1];
    diff[2] = _diff[2];
}

// --------------------- Randbedingungen der Teilchen -----------------------------

void Particle::x_border(double xleft, double xright) //xright > x > xleft
{
	double l = fabs(xleft - xright);
	_x -= l * floor(_x / l);
}

void Particle::y_border(double ydown, double yup)
{
	double l = fabs(ydown - yup);
	_y -= l * floor(_y / l);
}

void Particle::z_border(double zdown, double zup)
{
	double l = fabs(zdown - zup);
	_z -= l * floor(_z / l);
}

// --------------------- Streaming-Schritt & Beschleunigung -----------------------

void Particle::streaming(double time)
{
	_y += time * _v_y;
	_x += time * _v_x;
	_z += time * _v_z;
}

void Particle::acceleration(double time, double force_x)
{
	_v_x += force_x * time;
}

// --------------------- Kollision-Schritt und Shiften des Grids ------------------

void Particle::collisionSRD3D(double *v_mittel, double *R, double alpha, double kappa)
{
    double v_xtmp = _v_x,
    	   v_ytmp = _v_y,
    	   v_ztmp = _v_z;
    double R_x = R[0],
           R_y = R[1],
           R_z = R[2];
    ///Elemente der Matrix D_rot:
    double D_rot[3][3];
    D_rot[0][0] = R_x*R_x+(1.0-R_x*R_x)*cos(alpha);
    D_rot[0][1] = R_x*R_y*(1.0-cos(alpha)) - R_z*sin(alpha);
    D_rot[0][2] = R_x*R_z*(1.0-cos(alpha)) + R_y*sin(alpha);
    D_rot[1][0] = R_x*R_y*(1.0-cos(alpha)) + R_z*sin(alpha);
    D_rot[1][1] = R_y*R_y+(1.0-R_y*R_y)*cos(alpha);
    D_rot[1][2] = R_y*R_z*(1.0-cos(alpha)) - R_x*sin(alpha);
    D_rot[2][0] = R_x*R_z*(1.0-cos(alpha)) - R_y*sin(alpha);
    D_rot[2][1] = R_y*R_z*(1.0-cos(alpha)) + R_x*sin(alpha);
    D_rot[2][2] = R_z*R_z+(1.0-R_z*R_z)*cos(alpha);
    _v_x = v_mittel[0] + D_rot[0][0] * kappa * (v_xtmp - v_mittel[0])
    	               + D_rot[0][1] * kappa * (v_ytmp - v_mittel[1])
    	               + D_rot[0][2] * kappa * (v_ztmp - v_mittel[2]);
    _v_y = v_mittel[1] + D_rot[1][0] * kappa * (v_xtmp - v_mittel[0])
    	               + D_rot[1][1] * kappa * (v_ytmp - v_mittel[1])
    	               + D_rot[1][2] * kappa * (v_ztmp - v_mittel[2]);
    _v_z = v_mittel[2] + D_rot[2][0] * kappa * (v_xtmp - v_mittel[0])
    	               + D_rot[2][1] * kappa * (v_ytmp - v_mittel[1])
    	               + D_rot[2][2] * kappa * (v_ztmp - v_mittel[2]);
}

void Particle::shift(tuple<double> a0, double xleft, double xright, double zdown, double zup, double ydown, double yup)
{
	double l_x = fabs(xleft - xright);
	double l_z = fabs(zdown - zup);
	double l_y = fabs(ydown - yup);
	_x += a0.x;
	if (_x < xleft) _x += l_x;
	else if (_x > xright) _x -= l_x;
	_y += a0.y;
	if (_y < ydown) _y += l_y;
	else if (_y > yup) _y -= l_y;
	_z += a0.z;
	if (_z < zdown) _z += l_z;
	else if (_z > zup) _z -= l_z;
}

void Particle::prop(double propulsion, double M, double diff[])
{
    SetDiff(diff);
    if (_mass > 1)
    {
    	propulsion *= -M / _mass;
    }
    _v_x += propulsion / norm1(_diff) * _diff[0];
    _v_y += propulsion / norm1(_diff) * _diff[1];
    _v_z += propulsion / norm1(_diff) * _diff[2];
}
