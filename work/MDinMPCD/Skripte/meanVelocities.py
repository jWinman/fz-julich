import matplotlib.pyplot as plt
import numpy as np
import sys

def normalize(v):
    norm=np.linalg.norm(v)
    if norm==0:
       return v
    return v/norm

deltaT = np.arange(1, 500, 1)
va = np.zeros(len(deltaT))
vb = np.zeros(len(deltaT))

x1, y1, z1 = np.loadtxt("Daten/Positions/positions-{}-0.csv".format(sys.argv[1]), unpack=True)
x2, y2, z2 = np.loadtxt("Daten/Positions/positions-{}-1.csv".format(sys.argv[1]), unpack=True)

x_cm = (x1 + x2) / 2
y_cm = (y1 + y2) / 2
z_cm = (z1 + z2) / 2

diff = np.array([x2 - x1, y2 - y1, z2 - z1]).T
diff = np.array([normalize(diffi) for diffi in diff])

j = 0
for t in deltaT:
    vxa_cm = np.array([(x_cm[i + t] - x_cm[i]) / (t * 0.1) for i in range(len(x_cm) - t)])
    vya_cm = np.array([(y_cm[i + t] - y_cm[i]) / (t * 0.1) for i in range(len(y_cm) - t)])
    vza_cm = np.array([(z_cm[i + t] - z_cm[i]) / (t * 0.1) for i in range(len(z_cm) - t)])
    va[j] = np.mean(np.sqrt(vxa_cm**2 + vya_cm**2 + vza_cm**2))

    vxb_cm = np.array([(x_cm[i + t] - x_cm[i]) * diff[i + t // 2][0] / (t * 0.1) for i in range(len(x_cm) - t)])
    vyb_cm = np.array([(y_cm[i + t] - y_cm[i]) * diff[i + t // 2][1] / (t * 0.1) for i in range(len(y_cm) - t)])
    vzb_cm = np.array([(z_cm[i + t] - z_cm[i]) * diff[i + t // 2][2] / (t * 0.1) for i in range(len(z_cm) - t)])
    vb[j] = np.mean(vxb_cm + vyb_cm + vzb_cm)
    j += 1

deltaT_und_va = np.array([deltaT, va]).T
deltaT_und_vb = np.array([deltaT, vb]).T

np.savetxt("Daten/meanVelocities/meanVelocitiesA{}.csv".format(sys.argv[1]), deltaT_und_va)
np.savetxt("Daten/meanVelocities/meanVelocitiesB{}.csv".format(sys.argv[1]), deltaT_und_vb)
