import numpy as np
import sys

#jens: multi dimensional msd
def msdisplacemd(vecs, x_values):
   '''This function calculates mean squared displacement for a 1 d vec
   vecs: position values, needs to be type array(array) or list(array)
   x_values array of dt to study
   '''
   m = np.array([])
   l = len(vecs)
   for t in x_values:
       tmp = 0
       tmpKorr = 0
       n = l - t
       for i in range(0,n):
           tmp += np.dot((vecs[t+i]-vecs[i]),(vecs[t+i]-vecs[i]))
       tmp /= n
       m = np.append(m,tmp)
   return x_values, m

r_xyz = np.loadtxt("Daten/Positions/positions-{}-0.csv".format(sys.argv[1]))
t = np.concatenate([np.arange(0, 100, 2), np.arange(100, 1000, 10)])
MSD = np.array(msdisplacemd(r_xyz[10000:], t))

np.savetxt("Daten/MSD/MSD{}.csv".format(sys.argv[1]), MSD.T, delimiter=' ', newline='\n')
