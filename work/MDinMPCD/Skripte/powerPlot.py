import matplotlib.pyplot as plt
import numpy as np

def normalize(v):
    norm=np.linalg.norm(v)
    if norm==0:
       return v
    return v/norm

coinStrs = ["0.03", "0.05", "0.07", "0.10", "0.20", "0.30", "0.40", "0.50", "0.70", "0.80", "0.90", "1.00", "1.20", "1.40", "1.60"]
coins = np.array([0.03, 0.05, 0.07, 0.10, 0.20, 0.30, 0.40, 0.50, 0.7, 0.8, 0.9, 1.0, 1.2, 1.4, 1.6])
v0 = np.zeros(len(coins))
v0andPowerCoins = np.zeros((len(coins), 4))

i = 0
for coin, coinInt in zip(coinStrs, coins):
    # Position laden
    x1, y1, z1 = np.loadtxt("Daten/Positions/positions-{}-0.csv".format(coin), unpack=True)
    x2, y2, z2 = np.loadtxt("Daten/Positions/positions-{}-1.csv".format(coin), unpack=True)

    # Velocities laden
    v1x, v1y, v1z = np.loadtxt("Daten/Velocities/velocities-{}-0.csv".format(coin), unpack=True)
    v2x, v2y, v2z = np.loadtxt("Daten/Velocities/velocities-{}-1.csv".format(coin), unpack=True)

    # Energies laden
    _, deltaEIn, deltaEOut, deltaEtot = np.loadtxt("Daten/DeltaE/DeltaE-{}.csv".format(coin), unpack=True)

    # Differenz zwischen Particles berechnen
    diff = np.array([x2 - x1, y2 - y1, z2 - z1]).T
    diff = np.array([normalize(diffi) for diffi in diff])
    diffx = diff[:,0]
    diffy = diff[:,1]
    diffz = diff[:,2]

    v_xmean = (v1x + v2x) / 2
    v_ymean = (v1y + v2y) / 2
    v_zmean = (v1z + v2z) / 2

    v0andPowerCoins[i][0] = np.mean(v_xmean * diffx + v_ymean * diffy + v_zmean * diffz)
    v0andPowerCoins[i][1] = sum(deltaEIn) / (0.1 * 110000)
    v0andPowerCoins[i][2] = sum(deltaEOut) / (0.1 * 110000)
    v0andPowerCoins[i][3] = sum(deltaEtot) / (0.1 * 110000)
    i += 1

np.savetxt("Daten/v0/Power.csv", v0andPowerCoins)
