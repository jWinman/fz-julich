import numpy as np
import sys

def normalize(v):
    norm=np.linalg.norm(v)
    if norm==0:
       return v
    return v/norm

#jens: multi dimensional msd
def oKorr(vecs, x_values):
   '''This function calculates mean squared displacement for a 1 d vec
   vecs: position values, needs to be type array(array) or list(array)
   x_values array of dt to study
   '''
   m = np.array([])
   l = len(vecs)
   for t in x_values:
       tmp = 0
       tmpKorr = 0
       n = l - t
       for i in range(0,n):
           tmp += np.dot(vecs[t+i], vecs[i])
       tmp /= n
       m = np.append(m,tmp)
   return x_values, m

x1, y1, z1 = np.loadtxt("Daten/Positions/positions-{}-0.csv".format(sys.argv[1]), unpack=True)
x2, y2, z2 = np.loadtxt("Daten/Positions/positions-{}-1.csv".format(sys.argv[1]), unpack=True)

diff = np.array([x1 - x2, y1 - y2, z1 - z2]).T
diff = np.array([normalize(diffi) for diffi in diff])

t = np.arange(0, 1000, 5)
oKorr = np.array(oKorr(diff[10000:], t))

np.savetxt("Daten/orientationKorr/oKorr{}.csv".format(sys.argv[1]), oKorr.T, delimiter=' ', newline='\n')
