#ifndef TUPLE_H
#define TUPLE_H

template <typename T>
struct tuple
{
    T x, y, z;
};

template <typename T>
tuple<T> inverse(tuple<T> _shift)
{
    return {-_shift.x,
	    -_shift.y,
	    -_shift.z};
}

#endif
