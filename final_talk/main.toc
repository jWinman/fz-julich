\beamer@endinputifotherversion {3.33pt}
\select@language {ngerman}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{The simulation methods: MPC and MD}{5}{0}{2}
\beamer@sectionintoc {3}{The propulsion of the dumbbell}{15}{0}{3}
\beamer@sectionintoc {4}{Results}{21}{0}{4}
