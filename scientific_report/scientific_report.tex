\documentclass{article}
\usepackage{sr2009}
\usepackage{floatflt} 
\include{headerbach}
\begin{document}
%--------------------------------------------------------------------
% HEAD
%--------------------------------------------------------------------
\begin{screphead}
\screptitle{An active dumbbell in a mesoscopic hydrodynamic simulation}
\begin{screpauthors}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Here are two different ways to create your Author-Institute list.
% A) USE WHEN SOME AUTHORS ARE CONNECTED TO DIFFERENT INSTITUTIONS
% B) USE ONLY WHEN EVERY AUTHOR IS CONNECTED TO ONLY ONE INSTITUTION
%
% USE A) OR B) COMPLETLY and don`t try to use both
% A) UP TO 20 AUTHORS
% B) UP TO 10 AUTHORS
%
% you need to comment out the unnecessary WAY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
% START A) 
% \srauthor{NAME}{NUMBERS} //equal with numbers on authoring
% \srauthoring{NAME}{NUMBER} 
% 
\noautocalc
\srauthor{J.~Winkelmann}{1}
\srauthor{J.~Elgeti}{1}
\srauthor{R.~G.~Winkler}{1}
\srauthoring{\IAS}{1}
% END A)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START B)
% \authitem{Author}{Institution}
% Maximal 10 authitems / max 5 Institutes
%
% You can also use:	\authitem{Author}{et. al.} if there is a printoverlay
%\authitem{J.~Bloggs}{\IFFnine}
%\authitem{F.~Bloggs}{\JCNS}
%\authitem{E.~Ibm}{Institute of Electronic Materials II, RWTH Aachen University}
%\authitem{E.~Ibm}{Please use the official, or if applicable, the English institute names}
% END B)
\end{screpauthors}
\end{screphead}

%-----------------------------------------------------------------------
% BODY (abstract & text including max. 5 figures/tables)
%----------------------------------------------------------------------

\begin{screpbody}
\begin{screpabstract}
Active and self-propelled particles play an important role in investigating the behavior of microswimmers like bacteria in fluids.
These can model different aspects of microswimmers, such as single particle motion or collective behaviors, and therefore give an understanding of the physical mechanisms behind the self-propelled motion.
We want to model such an active particle in a fluid by describing the particle with a dumbbell which consists of an active and passive particle, that are linked together with a spring.
The fluid is simulated with an mesoscopic hydrodynamic simulation called Multiparticle Collision Dynamics (MPCD) and the dumbbell with Molecular Dynamic (MD).
We measured the mean square displacement and the velocity depending on the propulsion for an isolated dumbbell.
\end{screpabstract}

\begin{screptext}
%\paragraph{The simulation methods:}
MPC is a particle based mesoscopic simulation model to simulate solvents with $N_{\mathrm{s}} = \num{10000}$ point particles of mass $m = 1$.
For this method the simulation box of length $L = 10\,a$ and periodic boundaries is divided into cells with a cell constant $a = 1$ and the time is discretized in time steps $\Updelta t = 0.1$
In every time step a streaming and collision step is implemented.
The streaming step calculates the ballistic motion of the $i$th particle
\begin{equation}
\vec{r}_i (t + \Updelta t) = \vec{r}_i (t) + \Updelta t \, \vec{v}_i(t)\,.
\end{equation}
For the collision step we used the Stochastic Rotation Dynamics (SRD) which rotates the relative velocity $\updelta \vec{v}_i = \vec{v}_i(t) - \vec{v}_{c,\mathrm{cm}}(t)$ with a stochastic matrix $\vec{R}_c$ 
\begin{equation}
\vec{v}_i (t + \Updelta t)= \vec{v}_{c,\mathrm{cm}} (t) + \vec{R}_{c}\, \updelta \vec{v}_i (t)
%\quad \updelta \vec{v}_i(t) = \vec{v}_i(t) - \vec{v}_{c,\mathrm{cm}}(t)\,.
\end{equation}
Therefore the center of mass velocity $\vec{v}_{c, \mathrm{cm}}$ is calculated for the cell $c$ as
\begin{equation}
v_{c, \mathrm{cm}} = \frac{\sum_{i=1}^{N^c_{\mathrm{s}}} m \vec{v}_i(t) + \sum_{k = 1}^{N^c_{\mathrm{m}}} M \vec{v}_k(t)}{m N^c_{\mathrm{m}} + M N^c_{\mathrm{m}}}
\end{equation}
where $N^c_{\mathrm{s}}$ is the number of solvent particles in the cell and $N^c_{\mathrm{m}}$ the number of solute particles with mass $M$.

For the simulating the dumbbell with the MD we use a spring with the potential
\begin{equation}
U_b = \frac{k}{2} (\abs{\vec{r}_1 - \vec{r}_2} - l)^2
\end{equation}
to link the two particles with positions $\vec{r}_1$ and $\vec{r}_2$ and each of a mass $M_{\mathrm{dumb}} = \nicefrac{N_s}{L^3} = 10$.
For the length $l = 2\, a$ of the dumbbell we used a length that is longer than one cell but short enough for hydrodynamic interaction between the two particles and the spring constant $k = 1000$ is chosen to be very stiff.
For integrating the equation of motion for the dumbbell a Verlet algorithm with $\Updelta t = 0.005$ is implemented.

%\paragraph{The propulsion of the dumbbell:}
For the self propulsion of the dumbbell we used a momentum conserved propulsion step which is illustrated in figure \ref{fig1}.
All the fluid particles get a propulsion $\vec{\alpha}$ with the opposite direction than the direction of the dumbbell $\vec{e}_{\mathrm{dumb}}$.
By adding up all these propulsion and dividing it by $M_{\mathrm{dumb}}$, a momentum conserved propulsion is calculated
\begin{equation}
\vec{\alpha}_{\mathrm{dumb}} = - \frac{N^c_{\mathrm{m}} \alpha}{M_{\mathrm{dumb}}} = \alpha_{\mathrm{dumb}} \cdot \vec{e}_{\mathrm{dumb}}\,.
\end{equation}
%The direction of this propulsion is $\vec{e}_{\mathrm{dumb}}$ and the dumbbell forms a pusher.

\begin{figure}[h!]
\centering
\vspace{0.5cm}
\includegraphics[width=4cm]{propulsion}
\caption
{
\srcaptionfont
{
The propulsion of the dumbbell (red particles) in the fluid (black particles).
The fluid particles get a propulsion of $\vec{\alpha}$ and the propulsion of the dumbbell is $\vec{\alpha}_{\mathrm{dumb}}$.
Both propulsions are connected via momentum conservation.
}
}
\label{fig1}
\end{figure}

%\paragraph{Results and discussion:}
First, we measured the mean square displacement (MSD), $\avg{(\vec{r}(t + t_0) - \vec{r}(t_0))^2}$.
The measured values for different propulsions $\alpha$ are displayed in figure \ref{fig2} for small times $t$.
For the fit function we used a function
\begin{align}
\mathrm{MSD}(t)= &6 \frac{k_{\mathrm{B}} T}{\gamma} t - \frac{6k_{\mathrm{B}} T M_{\mathrm{dumb}}}{\gamma^2} \left( 1 - \e^{-\frac{\gamma t}{M_{\mathrm{dumb}}}} \right) \\
+ & 2 \tau_r^2 v_0^2 \left(\frac{t}{\tau_r} + \e^{-\frac{t}{\tau_r}} - 1\right)
\end{align}
that predicts the theoretical values of the MSD for active particles and consists of an active and a passive
term \cite{microswimmer}.
%The passive term reflects the brownian motion of the dumbbell.
This function depends on the friction coefficient $\gamma$ and the rotational diffusion relaxation time $\tau_r$ which was calculated externally.

\begin{figure}[h!]
\centering
\vspace{0.5cm}
\includegraphics[width=6cm]{MSDcoins}
\caption
{
\srcaptionfont
{
The MSD for different propulsions $\alpha$ and small times $t$.
The solid lines display a fit function with the velocity $v_0$ as a fit parameter.
}
}
\label{fig2}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[width=5.5cm]{meanVelocitiesA}
\includegraphics[width=5.5cm]{meanVelocitiesB}

\caption
{
\srcaptionfont
{
Calculation of the velocity with a forward difference approximation depending on $\Updelta t$.
In the upper plot the absolute value of the velocity is taken, in the lower plot the velocity is projected in the direction $\vec{e}_{\mathrm{dumb}}$ before averaged over all times $t$.
For comparison, $v_0$ from the MSD$(t)$ fit is plotted with dashed lines.
}
}
\label{fig3}
\end{figure}

Next, the velocity of the dumbbell was calculated with a forward difference approximation with two different methods (fig. \ref{fig3}).
For the first method (upper plot of \ref{fig3}) the absolute value of the velocity is taken before averaging over all times.
The comparison with the MSD velocities, which are displayed with the dashed lines, shows that there are $\Updelta t$ where the calculated velocities coincide with the MSD velocities.
But since the region is small and not the same for every $\alpha$, a statement concerning the velocity cannot be made.
In the second method (lower plot of \ref{fig3}) the velocity is projected into $\vec{e}_{\mathrm{dumb}}$ and then averaged.
Here the comparison with the MSD velocities shows that $\avg{\vec{v}(\Updelta t) \cdot \vec{e}_{\mathrm{dumb}}}$ coincide for small $\Updelta t$ and then the calculated velocities decrease.
So an approximation of the velocity can be made by averaging over the first $\avg{\vec{v}(\Updelta t) \cdot \vec{e}_{\mathrm{dumb}}}$ for small $\Updelta t$.
This velocity together with the MSD velocity is plotted against the propulsion $\alpha$ in \ref{fig4}.
A third velocity for this plot was measured by averaging over the velocities $\vec{v}_1$ and $\vec{v}_2$ of the two particles of the dumbbell, $v_0 = \Avg{\frac{\vec{v}_1 + \vec{v}_2}{2} \cdot \vec{e}_{\mathrm{dumb}}}$.
The plot shows that all calculated velocities coincide for all plotted $\alpha$.
For small propulsions a linear response between the propulsion and the measured velocity is observed with a slope of $\num{0.891}$.
From this one can conclude that the propulsion is almost completely converted into the velocity of the dumbbell.
For higher propulsions the velocity begins to decrease.
This is due to the fact that the velocities reaches the speed of sound $c$ of the fluid.
By assuming that the MPC fluid is an ideal gas, $c$ can be calculated over the ideal gas law and $c = \sqrt{\nicefrac{C_p}{C_V} \cdot \nicefrac{\dx p}{\dx \rho}} = \sqrt{\nicefrac{5 k_{\mathrm{B}}T}{3 m}} \approx 1.3$ \cite{sound}.
$\rho$ is the mass density of the ideal gas.
Thus higher propulsion are not of so much interest for further studies.

In further studies one can investigate the length dependence of the slope where one would expect the slope to decrease with increasing length.
Also the calculated velocities can be measured for different powers $\avg{P}$ of the dumbbell.
Besides single particle properties, collective properties of a system with dumbbells can be examined in the distant future.

\begin{figure}[t]
\centering
\includegraphics[width=5.5cm]{powerplot}

\caption
{
\srcaptionfont
{
The velocity of the dumbbell depending on $\alpha$ calculated with three different methods, the MSD, the forward different approximation and averaging over the velocities of the particles of the dumbbell.
The dashed line is a linear fit for velocities with $\alpha < 0.5$.
}
}
\label{fig4}
\end{figure}
\end{screptext}

%------------------------------------------------------------------------
% BIBLIOGRAPHY
%-----------------------------------------------------------------------
\begin{screpbibliography}
%
% in the text use: \cite{key}
%
% here use \srbibitem{key}{TEXT}
%\srbibitem{auth05}{T. Auth and G. Gompper, Phys. Rev. E {\bf 72}, 031904 (2005).}
\srbibitem{thermostat}{R.~G.~Winkler, \em{Lecture Notes from the IAS Winter School on `Hierarchical Methods for Dynamics in Complex Molecular Systems Lecture Notes'}, chap. Flow Simulations with Multiparticle Collision Dynamics}
\srbibitem{mlcd}{M.~Ripoll, \em{Lecture Notes from the 42th IFF Spring School on `Macromolecular Systems in Soft and Living Matter'}, chap. Mesoscale Hydrodynamics}
\srbibitem{cooperative}{A.~Wysocki and R.~G.~Winkler and G.~Gompper, EPL, 105, 48004, (2014).}
\srbibitem{microswimmer}{J.~Elgeti and R.~G.~Winkler and G.~Gompper, \em Physics of Microswimmers - Single Particle Motion and Collective Behavior.}
\srbibitem{sound}{A.~Lamura, G.~Gompper, T.~Ihle, D.~M.~Kroll, Europhys. Lett. 56 (3), pp. 319-325 (2001)}
\end{screpbibliography}
\end{screpbody}
\end{document}